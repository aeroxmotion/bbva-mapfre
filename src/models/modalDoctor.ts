export class ModalDoctor {
    idType: number;
    nif: number;
    namePac: string;
    address: string;
    email: string;
    cel: number;
}