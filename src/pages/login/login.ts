import { Component  } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import { CreateUserPage } from '../create-user/create-user';
import { ResetPassPage } from '../reset-pass/reset-pass';
import { ChangePassPage } from '../change-pass/change-pass';
import jsSHA from 'jssha';
import { HomePage } from '../home/home';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    /*
    * firstAccess
    * 0 => ya se ha entrado varias veces a la app
    * 1 => primera vez
    */
    loginForm;
    page: string = 'login';
    data: any;
    pass: any;
    res: any;
    resOk: any;
    status: boolean;
    message: string;
    logo: any;
    loading: boolean;
    item: any;
    list: any;
    dataTwo: any;
    items: any;
    deferredPrompt;
    showBtn: boolean = false;
    pages: any = {"type": "form", "name": "login", "title": "Ingresa tus datos para comenzar", "buttonLogin": "Continuar", "buttonRegister": "Registrese", "buttonReset": "Olvidaste tu contraseña?", "img": [ { "url": "assets/imgs/512906076.jpg", "title": "", "subTitle": "Es muy sencillo asegurar tus sueños y los de tu familia" }, { "url": "assets/imgs/523171462.jpg", "title": "", "subTitle": "Es muy sencillo asegurar tus sueños y los de tu familia" }, {  "url": "assets/imgs/625683286.jpg", "title": "", "subTitle": "Es muy sencillo asegurar tus sueños y los de tu familia" } ] };
    private captchaPassed: boolean = false;
    private captchaResponse: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public ConfigProvider: ConfigProvider,
        public LoginProvider: LoginProvider,
        public alertCtrl: AlertController,
        //private zone: NgZone
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.logo = this.ConfigProvider.logo();
        this.loading = true;
        if (!this.data) {
            this.data = this.pages;
            this.logo = 'assets/imgs/bbva-logo.png';
        }
        this.ConfigProvider.init().subscribe(
            (data) => {
                this.ConfigProvider.saveLocal(data);
            }
        );
    }

    ngOnInit() {
        this.loading = false;
        this.loginForm = this.formBuilder.group({
            usuario: ['', Validators.required],
            contrasena: ['', Validators.required],
        });
    }

    ionViewWillEnter() {
        window.addEventListener('beforeinstallprompt', (e) => {
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            e.preventDefault();
            // Stash the event so it can be triggered later on the button event.
            this.deferredPrompt = e;
            // Update UI by showing a button to notify the user they can add to home screen
            this.showBtn = true;
        });

        //button click event to show the promt

        window.addEventListener('appinstalled', (event) => {
            alert('instalado');
        });

        /*
        if (window.matchMedia('(display-mode: standalone)').matches) {
            alert('display-mode is standalone');
        }
        */
    }

    add_to_home(e) {
        if (this.showBtn) {
            // hide our user interface that shows our button
            // Show the prompt
            this.deferredPrompt.prompt();
            // Wait for the user to respond to the prompt
            this.deferredPrompt.userChoice
                .then((choiceResult) => {
                    if (choiceResult.outcome === 'accepted') {
                        alert('Aplicación instalada con exito.');
                    } /*else {
                        alert('La aplicación no sera instalada.');
                    }*/
                    this.deferredPrompt = null;
                });
        } else {
            console.log('Is install');
        }
    }

    captchaResolved(response: string): void {
        this.captchaPassed = true;
        this.captchaResponse = response;
        console.log(this.captchaResponse);
    }
    
    continuar(response) {
        this.captchaResolved(response);

        if (this.captchaPassed && this.loginForm.valid)
        {
            this.loading = true;
            let usuario = this.loginForm.value.usuario;
            let contrasena = this.loginForm.value.contrasena;

            let bytes = this.str2ab(contrasena);
            let pass = new jsSHA('SHA-512', 'TEXT');
            contrasena = this.ab2str(bytes);
            pass.update(contrasena);
            let hash = pass.getHash('B64');
            this.LoginProvider.login(usuario, hash).subscribe(
                (result) => {
                    if (result['ERROR']) {
                        this.loading = false;
                        this.alert(result['ERROR']);
                    } else if (result['OK'] || result['root']) {
                        if (result['root'].firstAccess === 0) {
                            localStorage.setItem('token', result['root'].token);
                            localStorage.setItem('id', result['root'].idRegUsuario);
                            //this.updateSesion(result['root'].token);
                            this.loading = false;
                            this.navCtrl.setRoot(HomePage);
                        } else {
                            localStorage.setItem('oldPassword', hash);
                            localStorage.setItem('login', usuario);
                            localStorage.setItem('token', result['root'].token);
                            //this.updateSesion(result['root'].token);
                            this.loading = false;
                            this.navCtrl.setRoot(ChangePassPage);
                        }
                    } else if (result['WARNING']) {
                        localStorage.setItem('oldPassword', hash);
                        localStorage.setItem('login', usuario);
                        this.navCtrl.setRoot(ChangePassPage);
                    }
                }, (error) => {
                    console.log(error);
                }
            );
        }
        else
        {

        }
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }

    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'ok'
                }
            ]
        });
        alert.present();
    }

    register() {
        this.navCtrl.push(CreateUserPage);
    }

    olvido() {
        this.navCtrl.push(ResetPassPage);
    }

    updateSesion(token) {
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.LoginProvider.updateSesion(token, urls['json']).subscribe(
                    (result) => {
                        if (result['OK']) {
                            localStorage.setItem('exp', result['OK']);
                        } else {
                            localStorage.setItem('exp', result['OK']);
                        }
                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        );
    }

}
