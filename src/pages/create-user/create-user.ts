import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

/* PAGES */
import { LoginPage } from '../login/login';
import { InforTemplatePage } from '../infor-template/infor-template';
import { CallTemplatePage } from '../call-template/call-template';

/*PRIVIDER DE LOGIN*/
import { LoginProvider } from '../../providers/login/login';
import { CreateUserProvider } from '../../providers/create-user/create-user';
import { ConfigProvider } from '../../providers/config/config';

import jsSHA from 'jssha';

@IonicPage()
@Component({
    selector: 'page-create-user',
    templateUrl: 'create-user.html',
})
export class CreateUserPage {

    private formCreate;
    res: any;
    resOk: any;
    status: boolean;
    message: string;
    token: string;
    resCreate: any;
    resCreatOk: any;
    location: any;
    page: string = 'create-user';
    data: any;
    logo: any;
    loading: boolean;
    termsStatus: any;

    validation_messages = {
        'id': [
            { type: 'required', message: 'El id fiscal es requerido, este sera se Usuario.' },
            { type: 'minlength', message: 'El id fiscal debe tener al menos 4 caracteres.' },
            { type: 'maxlength', message: 'El id fiscal no debe tener mas de 25 caracteres.' }
        ],
        'name': [
            { type: 'required', message: 'El nombre es requerido.' },
        ],
        'lastName': [
            { type: 'required', message: 'El apellido es requerido.' },
        ],
        'email': [
            { type: 'required', message: 'El email es requerido.' },
            { type: 'pattern', message: 'No es un email valido.' },
        ],
        'termsAccepted': [
            { type: 'required', message: 'no ha aceptado los temrinos y condiciones.' },
        ]
    };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public LoginProvider: LoginProvider,
        public CreateUserProvider: CreateUserProvider,
        public alertCtrl: AlertController,
        public ConfigProvider: ConfigProvider
    ) {
        this.location = JSON.parse(localStorage.getItem('country'));
        this.data = this.ConfigProvider.selectData(this.page);
        this.logo = this.ConfigProvider.logo();
        this.loading = true;

    }

    ngOnInit() {
        this.loading = false;
        let reg = JSON.parse(localStorage.getItem('register'));
        if (reg === null) {
            this.formCreate = this.formBuilder.group({
                id: ['', Validators.required],
                name: ['', Validators.required],
                lastName: ['', Validators.required],
                email: ['', Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
                termsAccepted: [null, Validators.required]
            });
        } else {
            this.formCreate = this.formBuilder.group({
                id: ['', Validators.required],
                name: ['', Validators.required],
                lastName: ['', Validators.required],
                email: ['', Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
                termsAccepted: [null, Validators.required]
            });
        }
    }
    onTermsChecked($event) {
        if (!$event.checked) {
            this.formCreate.patchValue({ termsAccepted: null });
        }
    }
    /* REGISTRO */
    create() {
        this.formCreate.value.date = '01/01/1980';
        this.formCreate.value.gen = 5;
        this.loading = true;
        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['user'].superUser;
                let contrasena = dat['user'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');
                localStorage.removeItem('register');
                this.LoginProvider.login(usuario, hash).subscribe(
                    (result) => {
                        if (result['ERROR']) {
                            this.loading = false;
                            this.alert(result['ERROR']);
                        } else {
                            let token = result['root'].token;
                            let idRegDealer = result['root'].idRegDealer;
                            this.termsStatus = localStorage.getItem('terms');
                            if (this.termsStatus === 0) {
                                this.CreateUserProvider.createUser(token, this.formCreate.value).subscribe(
                                    (result) => {
                                        if (result['OK'] || result) {
                                            this.loading = false;
                                            this.alert(result['OK']);
                                            this.navCtrl.setRoot(LoginPage);
                                        } else if (result['ERROR']) {
                                            this.loading = false;
                                            this.alert(result['ERROR']);
                                        } else {
                                            this.loading = false;
                                            this.alert(result['WARNING']);
                                        }
                                    }, (error) => {
                                        console.log(error);
                                        this.alert('error');
                                    }
                                );
                            } else {
                                this.LoginProvider.terms(token, this.formCreate.value.id, idRegDealer).subscribe(
                                    (ter) => {
                                        if (ter['root'].tblContratos) {
                                            this.CreateUserProvider.createUser(token, this.formCreate.value).subscribe(
                                                (result) => {
                                                    if (result['OK'] || result) {
                                                        this.loading = false;
                                                        this.alert(result['OK']);
                                                        this.navCtrl.setRoot(LoginPage);
                                                    } else if (result['ERROR']) {
                                                        this.loading = false;
                                                        this.alert(result['ERROR']);
                                                    } else {
                                                        this.loading = false;
                                                        this.alert(result['WARNING']);
                                                    }
                                                }, (error) => {
                                                    console.log(error);
                                                    this.alert('error');
                                                }
                                            );
                                        } else {
                                            this.alertFail('No es posible crear su usuario, por favor comunícate con nuestro call center', 'contacto');
                                        }
                                    }, (error) => {
                                        console.log(error);
                                        this.alert('error');
                                    }
                                );
                            }
                        }

                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        );
    }

    /* CANCELAR */
    cancel() {
        this.navCtrl.push(LoginPage);
        localStorage.removeItem('register');
    }

    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'ok'
                }
            ]
        });
        alert.present();
    }

    terms() {
        let register = this.formCreate.value;
        localStorage.setItem('register', JSON.stringify(register));
        let data = this.ConfigProvider.selectData('terminos');
        data.back = 'create-user';
        data.login = false;
        this.navCtrl.setRoot(InforTemplatePage, {
            content: data
        });
    }

    alertFail(message, page) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            subTitle: message,
            buttons: [
                {
                    text: 'Aceptar',
                    handler: () => {
                        let contentPage = this.ConfigProvider.selectData(page);
                        contentPage.back = 'create-user';
                        this.navCtrl.setRoot(CallTemplatePage, {
                            content: contentPage
                        });
                    }
                }
            ]
        });
        alert.present();
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }
}
