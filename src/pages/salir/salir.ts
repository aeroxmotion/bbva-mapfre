import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PreRegistroPage } from '../pre-registro/pre-registro';

@IonicPage()
@Component({
    selector: 'page-salir',
    templateUrl: 'salir.html',
})
export class SalirPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        localStorage.removeItem('token');
        this.navCtrl.push(PreRegistroPage);
    }

}
