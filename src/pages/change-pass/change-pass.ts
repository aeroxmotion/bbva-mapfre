import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PasswordProvider } from '../../providers/password/password';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import jsSHA from 'jssha';
import { HomePage } from '../home/home';
import { InforTemplatePage } from '../infor-template/infor-template';
import { CallTemplatePage } from '../call-template/call-template';

@IonicPage()
@Component({
    selector: 'page-change-pass',
    templateUrl: 'change-pass.html',
})
export class ChangePassPage {

    private ChangeForm;
    logo: any;
    page: string = 'ChangePassPage';
    data: any;
    loading: boolean;
    termsStatus: any;
    validation_messages = {
        'newPass': [
                { type: 'required', message: 'Contraseña es requerida.' },
                { type: 'minlength', message: 'La contraseña debe tener al menos 8 caracteres.' },
                { type: 'maxlength', message: 'La contraseña no debe tener mas de 15 caracteres.' },
                { type: 'pattern', message: 'Su contraseña debe contener números y letras.' },
                //{ type: 'validUsername', message: 'Your username has already been taken.' }
            ],
        'confirmPass': [
            { type: 'required', message: 'Contraseña es requerida.' },
            { type: 'minlength', message: 'La contraseña debe tener al menos 8 caracteres.' },
            { type: 'maxlength', message: 'La contraseña no debe tener mas de 15 caracteres.' },
            { type: 'pattern', message: 'Su contraseña debe contener números y letras.' },
            //{ type: 'validUsername', message: 'Your username has already been taken.' }
        ]
    };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public PasswordProvider: PasswordProvider,
        public ConfigProvider: ConfigProvider,
        public LoginProvider: LoginProvider
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.logo = this.ConfigProvider.logo();
        this.loading = true;
    }

    ngOnInit() {
        this.loading = false;
        this.ChangeForm = this.formBuilder.group({
            newPass: ['', Validators.compose([
                Validators.minLength(8), 
                Validators.maxLength(15), 
                Validators.required, 
                Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
                //Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#.$($)$-$_])([a-zA-Z0-9$@$!%*?&#.$($)$-$_]|[^ ]){8,15}$')])],            
            ])],
                confirmPass: ['', Validators.compose([Validators.minLength(8), 
                Validators.required,
                Validators.minLength(8), 
                Validators.maxLength(15)
            ])],

            termsAccepted: [null, Validators.required],
        }, { validator: this.checkIfMatchingPasswords('newPass', 'confirmPass')});
    }

    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[passwordKey],
                passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value ! ===  passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({notEquivalent: true});
            }
            else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    }

    /* CAMBIAR CONTRASEÑA */
    changePass() {
        this.loading = true;
        if (this.ChangeForm.value.newPass  === this.ChangeForm.value.confirmPass) {

        } else {
            this.loading = false;
            this.alert('Las contraseñas no son iguales');
        }

        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['user'].superUser;
                let contrasena = dat['user'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');

                this.LoginProvider.login(usuario, hash).subscribe(
                    (result) => {
                        if (result['ERROR']) {
                            this.loading = false;
                            this.alert(result['ERROR']);
                            this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'contacto');
                        } else {
                            let id = localStorage.getItem('login');
                            let token = result['root'].token;
                            let idDealer = result['root'].idDealer;
                            this.termsStatus = localStorage.getItem('terms');
                            if (this.termsStatus  ===  0) {
                                let contrasena2 = this.ChangeForm.value.newPass;
                                let bytes2 = this.str2ab(contrasena2);
                                let pass2 = new jsSHA('SHA-512', 'TEXT');
                                contrasena2 = this.ab2str(bytes2);
                                pass2.update(contrasena2);
                                let hash2 = pass2.getHash('B64');
                                this.PasswordProvider.updatePass(hash2).subscribe(
                                    (result) => {
                                        if (result['OK'] || result || result['WARNING']) {
                                            this.alert('Su contraseña ha sido cambiada con éxito');
                                            let user = localStorage.getItem('login');
                                            this.LoginProvider.login(user, hash2).subscribe(
                                                (result) => {
                                                    if (result['ERROR']) {
                                                        this.loading = false;
                                                        this.alert(result['ERROR']);
                                                    } else {
                                                        localStorage.setItem('token', result['root'].token);
                                                        //this.updateSesion(result['root'].token);
                                                        localStorage.setItem('id', result['root'].idRegUsuario);
                                                        localStorage.removeItem('login');
                                                        localStorage.removeItem('oldPassword');
                                                        this.loading = false;

                                                        let userMo = {
                                                            id: result['root'].idRegUsuario,
                                                            cd: user,
                                                            email: ''
                                                        };
                                                        this.modify(userMo);

                                                        this.navCtrl.setRoot(HomePage);
                                                    }
                                                }
                                            );
                                        } else {
                                            this.alert(result['ERROR']);
                                            this.loading = false;
                                        }
                                    }, (error) => {
                                        console.log(error);
                                    }
                                );
                            } else {
                                this.LoginProvider.terms(token, id, idDealer).subscribe(
                                    (ter) => {
                                        if (ter) {
                                            let contrasena2 = this.ChangeForm.value.newPass;
                                            let bytes2 = this.str2ab(contrasena2);
                                            let pass2 = new jsSHA('SHA-512', 'TEXT');
                                            contrasena2 = this.ab2str(bytes2);
                                            pass2.update(contrasena2);
                                            let hash2 = pass2.getHash('B64');
                                            this.PasswordProvider.updatePass(hash2).subscribe(
                                                (result) => {
                                                    if (result['OK'] || result || result['WARNING']) {
                                                        this.alert('Su contraseña ha sido cambiada con éxito');
                                                        let user = localStorage.getItem('login');
                                                        this.LoginProvider.login(user, hash2).subscribe(
                                                            (result) => {
                                                                if (result['ERROR']) {
                                                                    this.loading = false;
                                                                    this.alert(result['ERROR']);
                                                                } else {
                                                                    localStorage.setItem('token', result['root'].token);
                                                                    //this.updateSesion(result['root'].token);
                                                                    localStorage.setItem('id', result['root'].idRegUsuario);
                                                                    localStorage.removeItem('login');
                                                                    localStorage.removeItem('oldPassword');
                                                                    this.loading = false;

                                                                    let userMo = {
                                                                        id: result['root'].idRegUsuario,
                                                                        cd: user,
                                                                        email: ''
                                                                    };
                                                                    this.modify(userMo);

                                                                    this.navCtrl.setRoot(HomePage);
                                                                }
                                                            }
                                                        );
                                                    } else {
                                                        this.alert(result['ERROR']);
                                                        this.loading = false;
                                                    }
                                                }, (error) => {
                                                    console.log(error);
                                                }
                                            );
                                        }
                                    }, (error) => {
                                        this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'contacto');
                                    }
                                );
                            }
                        }
                    }, (error) => {
                        this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'contacto');
                    }
                );
            }

        );
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }

    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'ok'
                }
            ]
        });
        alert.present();
    }

    updateSesion(token) {
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.LoginProvider.updateSesion(token, urls['json']).subscribe(
                    (result) => {
                        if (result['OK']) {
                            localStorage.setItem('exp', result['OK']);
                        } else {
                            localStorage.setItem('exp', result['OK']);
                        }
                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        );
    }

    onTermsChecked($event) {
        if (!$event.checked) {
            this.ChangeForm.patchValue({ termsAccepted: null });
        }
    }

    terms() {
        let data = this.ConfigProvider.selectData('terminos');
        data.back = 'ChangePassPage';
        data.login = false;
        this.navCtrl.setRoot(InforTemplatePage, {
            content: data
        });
    }

    alertFail(message, page) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            subTitle: message,
            buttons: [
                {
                    text: 'Aceptar',
                    handler: () => {
                        let contentPage = this.ConfigProvider.selectData(page);
                        this.navCtrl.setRoot(CallTemplatePage, {
                            content: contentPage
                        });
                    }
                }
            ]
        });
        alert.present();
    }

    modify(user) {
        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['user'].superUser;
                let contrasena = dat['user'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');

                this.LoginProvider.login(usuario, hash).subscribe(
                    (result) => {
                        if (result['ERROR']) {
                            this.loading = false;
                            this.alert(result['ERROR']);
                        } else {
                            let token = result['root'].token;
                            this.LoginProvider.modifyUserPlus(token, user).subscribe(
                                (res) => {
                                    if (res) {
                                        console.log(res);
                                    }
                                }
                            );
                        }

                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        );
    }
}
