import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';
import { ConfigProvider } from '../../providers/config/config';
import { MenuTemplatePage } from '../menu-template/menu-template';
import { CallTemplatePage } from '../call-template/call-template';
import { TabsPage } from '../tabs/tabs';

import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps, GoogleMap, } from '@ionic-native/google-maps';
import { MapsProvider } from '../../providers/maps/maps';

import { ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { google } from "google-maps";
import { ModalDocPage } from '../modal-doc/modal-doc';
declare var google: google;

@Component({
    selector: 'page-doctor-dom',
    templateUrl: 'doctor-dom.html',
})
export class DoctorDomPage {

    searchControl: FormControl;
    map: GoogleMap;
    page: string = 'doctorDomi';
    data: any;
    dataTwo: any;
    menu: any = [];

    lat: number;
    lng: number;

    lat_ori: number;
    lng_ori: number;

    latSearch: number;
    lngSearch: number;

    address: any;
    loading: boolean;
    autocomplete: any;
    position: any;
    elem: any;

    //@ViewChild("search")
    public searchElementRef: ElementRef;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        public googleMaps: GoogleMaps,
        private geolocation: Geolocation,
        public formBuilder: FormBuilder,
        public MapsProvider: MapsProvider
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.loading = true;
    }

    ngOnInit() {
        this.dataTwo = this.navParams.get('content');
        this.searchControl = new FormControl();
        this.getPosition();
        this.elem = <HTMLInputElement> document.getElementsByClassName('searchbar-input')[0];
        this.autocomplete = new google.maps.places.Autocomplete(this.elem);
        this.loading = false;
    }

    /* BUSCAR */
    solicitar() {
        this.loading = true;
        var input = <HTMLInputElement> document.getElementById('pac-input');
        let coor = JSON.parse(localStorage.getItem('dest'));
        this.initMap(coor);
        this.MapsProvider.map(coor.lat, coor.lon).subscribe(
            (ad) => {
                this.address = ad['results'][0].formatted_address;
                let o = JSON.parse(localStorage.getItem('dest'));
                let des = {
                    lat: o.lat,
                    lon: o.lon,
                    address: this.address
                };
                input.value = des.address;
                localStorage.setItem('dest', JSON.stringify(des));
                this.loading = false;
            }
        );
    }

    /* VER FORMULARIO MODAL*/
    viewForm() {
        this.loading = true;
        let des = JSON.parse(localStorage.getItem('dest'));
        if (des === null) {
            let pos = JSON.parse(localStorage.getItem('position'));
            this.MapsProvider.map(pos.lat, pos.lon).subscribe(
                (result) => {
                    let res = result['results'][0];
                    let dest = {
                        lat: res.geometry.location.lat,
                        lon: res.geometry.location.lng,
                        address: res.formatted_address
                    };
                    localStorage.setItem('dest', JSON.stringify(dest));
                    this.lat = res.geometry.location.lat;
                    this.lng = res.geometry.location.lng;
                    this.loading = false;
                    this.navCtrl.setRoot(ModalDocPage);
                }
            );
        } else {
            this.navCtrl.setRoot(ModalDocPage);
        }

    }

    /*VOLVER*/
    dismiss(data) {
        let contentPage = this.ConfigProvider.selectData(data);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(TabsPage);
        } else if (contentPage.type === "map") {
            this.navCtrl.setRoot(DoctorDomPage, {
                content: contentPage
            });
        }
    }

    getPosition(): any {
        var input = <HTMLInputElement> document.getElementById('pac-input');
        this.address = localStorage.getItem('address');
        this.geolocation.getCurrentPosition()
            .then(response => {
                this.lat_ori = response.coords.latitude;
                this.lng_ori = response.coords.longitude;
                this.MapsProvider.map(this.lat_ori, this.lng_ori).subscribe(
                    (res) => {
                        this.address = res['results'][0].formatted_address;
                        let dest = {
                            lat: response.coords.latitude,
                            lon: response.coords.longitude,
                            address: res['results'][0].formatted_address
                        };
                        input.value = dest.address;
                        localStorage.setItem('dest', JSON.stringify(dest));
                        this.lat = dest.lat;
                        this.lng = dest.lon;
                        this.initMap(dest);
                        this.loading = false;
                    }
                );
            })
            .catch(error => {
                console.log(error);
            });
    }

    initMap(data) {
        var map;
        var input = <HTMLInputElement> document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);

        var myLatlng = new google.maps.LatLng(data.lat, data.lon);
        var mapOptions = {
            zoom: 16,
            center: myLatlng
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        //var image = "assets/icon/wellness_icon.svg";
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
        });
        marker.setMap(map);

        // Añadir nuevo marcador
        google.maps.event.addListener(map, "click", function (ele) {
            var latlng = new google.maps.LatLng(ele.latLng.lat(), ele.latLng.lng());
            marker.setPosition(latlng);
            let dest = {
                lat: ele.latLng.lat(),
                lon: ele.latLng.lng()
            };
            this.lat = dest.lat;
            this.lng = dest.lon;
            localStorage.setItem('dest', JSON.stringify(dest));
            // Creación de la petición HTTP
            var req = new XMLHttpRequest();
            // Petición HTTP GET síncrona hacia el archivo fotos.json del servidor
            req.open("GET", `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.lat},${this.lng}&key=AIzaSyB7_2etUPei2P3900nj_mEOFZHowtSnL_o`, false);
            // Envío de la petición
            req.send(null);
            // Impresión por la consola de la respuesta recibida desde el servidor
            let pet = JSON.parse(req.responseText);
            console.log(pet['results'][1]['formatted_address']);
            var h = pet['results'][1]['formatted_address'];
            input.value = pet['results'][1]['formatted_address'];
            let desT = JSON.parse(localStorage.getItem('dest'));
            let desF = {
                lat: desT.lat,
                lon: desT.lon,
                address: h
            };
            localStorage.setItem('dest', JSON.stringify(desF));
        });

        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                marker.setPosition(place.geometry.location);
                let pos = {
                    lat: place.geometry.location.lat(),
                    lon: place.geometry.location.lng(),
                    address: place.name
                };
                localStorage.setItem('dest', JSON.stringify(pos));
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        this.loading = false;
    }

    clean() {
        var input = <HTMLInputElement> document.getElementById('pac-input');
        input.value = "";
    }
    getAddress(place: Object) {
        console.log(place);
        google.maps.event.addListener(this.autocomplete, 'place_changed', () => {
            let place = this.autocomplete.getPlace();
            this.position = {
                lat: place.geometry.location.lat(),
                lon: place.geometry.location.lng(),
                address: place['formatted_address']
            };
            this.initMap(this.position);
            localStorage.setItem('dest', JSON.stringify(this.position));
        });
    }

}