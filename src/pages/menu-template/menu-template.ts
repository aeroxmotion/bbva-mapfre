import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { CallTemplatePage } from '../call-template/call-template';
import { DoctorDomPage } from '../doctor-dom/doctor-dom';
import { HomePage } from '../home/home';
import { ConveniosPage } from '../convenios/convenios';

@Component({
    selector: 'page-menu-template',
    templateUrl: 'menu-template.html',
})
export class MenuTemplatePage {

    data: any;
    logo: any;
    menu: any = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider
    ) {
        this.logo = this.ConfigProvider.logo();
        this.data = [];
    }

    ngOnInit() {
        this.data = this.navParams.get('content');
        for (let nav of this.data.menu) {
            if (nav.status === 1) {
                this.menu.push(nav);
            }
        }
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        } else if (contentPage.type === "map") {
            this.navCtrl.setRoot(DoctorDomPage, {
                content: contentPage
            });
        } else if (contentPage.type === "filter") {
            this.navCtrl.setRoot(ConveniosPage, {
                content: contentPage
            });
        }
    }
}
