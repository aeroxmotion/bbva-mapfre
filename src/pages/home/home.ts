import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { MenuTemplatePage } from '../menu-template/menu-template';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import { CallTemplatePage } from '../call-template/call-template';
import { SeguimientoPage } from '../seguimiento/seguimiento';
import { PerfilPage } from '../perfil/perfil';
import { HistorialPage } from '../historial/historial';
import { ContactoPage } from '../contacto/contacto';
import { ListProductPage } from '../list-product/list-product';
import { LoginPage } from '../login/login';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    token: string;
    buttonAsistencia: any;
    page: string = 'home';
    data: any;
    menu: any = [];
    tabs: any;
    logo: any;
    loading: boolean;

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public ConfigProvider: ConfigProvider,
        public LoginProvider: LoginProvider,
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.logo = this.ConfigProvider.logo();
        this.tabs = JSON.parse(localStorage.getItem('menuTabs'));
        this.loading = true;
    }

    ngOnInit() {
        this.getToken();
        this.updateSesion(localStorage.getItem('token'));
        for (let nav of this.data.menu) {
            if (nav.status === 1) {
                this.menu.push(nav);
            }
        }
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        } else if (contentPage.type === "listProducts") {
            this.navCtrl.setRoot(ListProductPage, {
                content: contentPage
            });
        }
    }

    click(click) {
        if (click === "seguimiento") {
            this.navCtrl.setRoot(SeguimientoPage);
        } else if (click === "profile") {
            this.navCtrl.setRoot(PerfilPage);
        } else if (click === "history") {
            this.navCtrl.setRoot(HistorialPage);
        } else if (click === "contact") {
            this.navCtrl.setRoot(ContactoPage);
        }
    }

    /* EXISTE TOKEN */
    getToken() {
        if (!this.ConfigProvider.getToken()) {
            this.navCtrl.setRoot(LoginPage);
        }
    }

    updateSesion(token) {
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.LoginProvider.updateSesion(token, urls['json']).subscribe(
                    (result) => {
                        if (result['OK']) {
                            localStorage.setItem('exp', result['OK']);
                            this.detail(localStorage.getItem('token'), localStorage.getItem('id'));
                        } else {
                            localStorage.setItem('exp', result['OK']);
                        }
                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        );
    }

    detail(token, idRegUsuario) {
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.LoginProvider.getDetail(token, idRegUsuario, urls['json']).subscribe(
                    (result) => {
                        localStorage.setItem('user', JSON.stringify(result['root'].usuarios));
                        this.loading = false;
                    }, (error) => {
                        console.log(error);
                        this.loading = false;
                    }
                );
            });

    }
}