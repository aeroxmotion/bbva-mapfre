import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { HomePage } from '../home/home';

@Component({
    selector: 'page-vouchers',
    templateUrl: 'vouchers.html'
})
export class VouchersPage {

    constructor(
        public navCtrl: NavController,
        public ConfigProvider: ConfigProvider
    ) {

    }

    ngOnInit() {
        
    }

    func() {
        this.navCtrl.setRoot(HomePage);
    }
}