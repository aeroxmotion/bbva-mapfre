import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfigProvider } from '../../providers/config/config';

/* PAGES */
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

@Component({
    selector: 'page-pre-registro',
    templateUrl: 'pre-registro.html',
})
export class PreRegistroPage {

    private preLoginForm;
    token: string;
    countries: any;
    cities: any;
    page: string = 'pre-login';
    logo: any;
    data: any;
    users: any;

    constructor(
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
        public ConfigProvider: ConfigProvider,
    ) {
        this.token = localStorage.getItem('token');

        if (localStorage.getItem('country') && this.token !== null) {
            this.navCtrl.push(TabsPage);
        } else if (localStorage.getItem('country')) {
            this.navCtrl.push(LoginPage);
        }
        this.logo = this.ConfigProvider.logo();
        this.data = this.ConfigProvider.selectData(this.page);
    }

    ngOnInit() {
        this.preLoginForm = this.formBuilder.group({
            country: ['', Validators.required],
            city: ['', Validators.required],
        });
        this.countries = this.data.country;
    }

    selectCountry() {
        for (let cit of this.data.country) {
            if (cit.id === this.preLoginForm.value.country) {
                this.cities = cit.city;
            }
        }
    }

    continuar() {
        localStorage.setItem("country", JSON.stringify(this.preLoginForm.value));
        this.navCtrl.setRoot(LoginPage);
    }
}