import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { MenuTemplatePage } from '../menu-template/menu-template';
import { CallTemplatePage } from '../call-template/call-template';
import { ConveniosMapPage } from '../convenios-map/convenios-map';
import { HomePage } from '../home/home';
import { Filter } from '../../models/filter';
import moment from 'moment';

@Component({
    selector: 'page-convenios',
    templateUrl: 'convenios.html',
})
export class ConveniosPage {

    data: any;
    filter: Filter;
    text: string;
    call: boolean;
    icon: string;

    /* LISTAS */
    departLists: any;
    citiesList: any;
    serviceList: any;
    datailList: any;
    specialtyList: any;
    zonesList: any;

    /*VER CAMPOS*/
    viewButton: boolean = false;
    viewCity: boolean = false;
    viewService: boolean = false;

    /*NGMODELS*/
    department: any;
    city: any;
    service: any;
    specialty: any;
    zone: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
    ) {
        this.data = this.navParams.get('content');
    }

    ngOnInit() {
        this.filter = new Filter();
        this.ConfigProvider.departments().subscribe(
            (data) => {
                this.departLists = data;
            }
        );
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        }
    }

    /* FILTROS */
    changeDepartment() {
        if (this.department === '') {
            this.viewCity = false;
            this.viewButton = false;
            this.viewService = false;
        } else {
            this.ConfigProvider.getData(this.department).subscribe(
                (data) => {
                    this.citiesList = data;
                    this.viewButton = false;
                    this.viewCity = true;
                    this.viewService = false;
                }, (error) => {
                    this.text = this.data.call;
                    this.viewButton = true;
                    this.viewService = false;
                }
            );
        }
    }

    changeCity() {
        if (this.city === '') {
            this.viewService = false;
            this.viewButton = false;
        } else {
            this.ConfigProvider.getData(this.city).subscribe(
                (data) => {
                    this.serviceList = data;
                    this.viewService = true;
                    this.viewButton = false;
                    this.call = false;
                }, (error) => {
                    this.call = true;
                    this.text = this.data.call;
                    this.viewButton = true;
                    this.viewService = false;
                }
            );
        }
    }

    changeService() {
        for (let ser of this.serviceList) {
            if (ser.zones === this.service) {
                this.icon = ser.icon;
            }
        }
        if (this.service === '') {
            this.viewButton = false;
        } else {
            this.ConfigProvider.getData(this.service).subscribe(
                (data) => {
                    this.datailList = data;
                    this.text = this.data.submit;
                    this.call = false;
                    this.viewButton = true;
                    
                }, (error) => {
                    this.call = true;
                    this.text = this.data.call;
                    this.viewButton = true;
                }
            );
        }
    }

    /* SUBMIT*/
    submit() {
        let dep;
        let cit;
        for (let d of this.departLists) {
            if (d.cities === this.department) {
                dep = d.name;
            }
        }
        for (let c of this.citiesList) {
            if (c.city === this.city) {
                cit = c.name;
            }
        }

        for (let i = 0; i < this.datailList.length; i++) {
            this.datailList[i].icon = this.icon;
            this.datailList[i].city = `${dep}/${cit}`;
        }
        if (this.text === this.data.call) {
            let contentPage = this.ConfigProvider.selectData(this.data.fail);
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage,
                specialty: this.specialty,
                zone: this.zone
            });
        } else {
            let contentPage = this.ConfigProvider.selectData(this.data.ok);
            this.navCtrl.setRoot(ConveniosMapPage, {
                content: contentPage,
                map: this.datailList
            });
        }

    }

    addHistory(call, title) {
        let calls = {
            click: title,
            date: moment().format('DD/MM/YYYY'),
            hour: moment().format('HH:mm'),
            icon: 'assets/icon/Telephone.svg',
            page: name,
            tel: call
        };
        if (localStorage.getItem('calls') === null) {
            let callArray = [];
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        } else {
            let callArray = JSON.parse(localStorage.getItem('calls'));
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        }
    }
}
