import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { HomePage } from '../home/home';
import { CreateUserPage } from '../create-user/create-user';
import { ChangePassPage } from '../change-pass/change-pass';

@Component({
    selector: 'page-infor-template',
    templateUrl: 'infor-template.html',
})
export class InforTemplatePage {

    data: any = [];
    logo: any;
    menu: any = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider
    ) {
        this.logo = this.ConfigProvider.logo();
    }

    ngOnInit() {
        this.data = this.navParams.get('content');
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        } else if (contentPage.type === "form") {
            if (this.data.back === 'create-user') {
                this.navCtrl.setRoot(CreateUserPage);
            } else if (this.data.back === 'ChangePassPage') {
                this.navCtrl.setRoot(ChangePassPage);
            }
        }
    }
}
