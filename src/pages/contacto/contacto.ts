import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ConfigProvider } from '../../providers/config/config';
import moment from 'moment';

@Component({
    selector: 'page-contacto',
    templateUrl: 'contacto.html'
})
export class ContactoPage {

    imgs: any;
    page: string = 'contacto';
    data: any;
    logo: any;

    constructor(
        public navCtrl: NavController,
        public ConfigProvider: ConfigProvider,
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.logo = this.ConfigProvider.logo();
    }

    ngOnInit() {
        this.jsonImg();
    }

    func() {
        this.navCtrl.setRoot(HomePage);
    }

    jsonImg() {
        this.imgs = [
            {
                "url": "assets/imgs/banner.PNG"
            },
            {
                "url": "assets/imgs/banner.PNG"
            },
            {
                "url": "assets/imgs/banner.PNG"
            }
        ];
    }

    addFirebase(call, title, name) {
        let calls = {
            click: title,
            date: moment().format('DD/MM/YYYY'),
            hour: moment().format('HH:mm'),
            icon: 'assets/icon/Telephone.svg',
            page: name,
            tel: call
        };
        if (localStorage.getItem('calls') === null) {
            let callArray = [];
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        } else {
            let callArray = JSON.parse(localStorage.getItem('calls'));
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        }
    }
    
    formCall(data) {
        window.open(data);
    }
}
