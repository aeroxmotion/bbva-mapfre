import { Component } from '@angular/core';
import { NavController } from "ionic-angular";
//import { BasePage } from 'mapfre-framework';

@Component({
  selector: 'page-page',
  templateUrl: 'page.html'
})
export class Page {

  name: string = "Page";

  constructor(public nav: NavController) {

  }

  goBack(): void {
    this.nav.pop();
  }

}
