import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';
import { ContactoPage } from '../contacto/contacto';
import { HomePage } from '../home/home';
import { VouchersPage } from '../vouchers/vouchers';
import { HistorialPage } from '../historial/historial';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

    tab0 = HomePage;
    tab1 = VouchersPage;
    tab2 = PerfilPage;
    tab3 = HistorialPage;
    tab4 = ContactoPage;
    token: string;

    constructor(
        public navCtrl: NavController,
    ) {
    }
}
