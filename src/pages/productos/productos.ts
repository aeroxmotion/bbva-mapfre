import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListProdPage } from '../list-prod/list-prod';
import { ProductProvider } from '../../providers/product/product';
import { LoginProvider } from '../../providers/login/login';

@Component({
    selector: 'page-productos',
    templateUrl: 'productos.html'
})
export class ProductosPage {

    data: any;
    id: any;
    infor: any;
    dsTpProducto: any;
    dSTP: any;
    idReg: any;
    table: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private _ProductProvider: ProductProvider,
        private _LoginProvider: LoginProvider
    ) {
        this.data = this.navParams.get('content');
        this.id = this.navParams.get('id');
        this.infor = this.navParams.get('infor');
        this.dsTpProducto = this.navParams.get('dsTpProducto');
        this.dSTP = this.navParams.get('dSTP');
        this.idReg = this.navParams.get('idReg');
        this.token();
    }

    func(click) {
        this.data.back = 'products';
        this.navCtrl.setRoot(ListProdPage, {
            content: this.data,
            id: this.id,
            dsTpProducto: this.dsTpProducto,
            dSTP: this.dSTP,
            idReg: this.idReg
        });
    }

    token() {
        let superUser = 'LVIRACOA';
        let superPass = 'NSkP/TNEbOg3XVks5qW4mzYrSCQPl1kwKfzFcWC7YB19JW4x63Sosepfs42l8hsIdHOV8YC6PbHs2teVhpRIUw==';
        this._LoginProvider.login(superUser, superPass, 46).subscribe(
            (data) => {
                this.createTable(data['root'].token, this.infor.cd);
            }
        );
    }

    createTable(token, id) {
        this._ProductProvider.table(token, id).subscribe(
            (table) => {
                this.table = table['root'].limitParts.limitPart;
            }
        );
    }

    conGen(conGenerales, cd) {
        let superUser = 'LVIRACOA';
        let superPass = 'NSkP/TNEbOg3XVks5qW4mzYrSCQPl1kwKfzFcWC7YB19JW4x63Sosepfs42l8hsIdHOV8YC6PbHs2teVhpRIUw==';
        this._LoginProvider.login(superUser, superPass, 46).subscribe(
            (data) => {
                console.log(conGenerales, cd, data['root'].token);
            }
        );
    }
}