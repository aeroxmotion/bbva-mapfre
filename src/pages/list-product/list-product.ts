import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import { ProductProvider } from '../../providers/product/product';
import { HomePage } from '../home/home';
import { ListSubProductPage } from '../list-sub-product/list-sub-product';
import jsSHA from 'jssha';

@Component({
    selector: 'page-list-product',
    templateUrl: 'list-product.html',
})
export class ListProductPage {

    data: any;
    logo: any;
    tok: string;
    loading: boolean;
    listOne = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        private _LoginProvider: LoginProvider,
        private _ProductProvider: ProductProvider
    ) {
        this.logo = this.ConfigProvider.logo();
        this.loading = true;
    }

    ngOnInit() {
        console.log(2);
        this.data = this.navParams.get('content');
        this.token();
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        }
    }

    token() {
        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['userProduct'].superUser;
                let contrasena = dat['userProduct'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');
                this._LoginProvider.login(usuario, hash, dat['userProduct'].country).subscribe(
                    (data) => {
                        this.tok = data['root'].token;
                        this.listP(this.tok);
                    }
                );
            }
        );
    }

    listP(token) {
        this._ProductProvider.list(token).subscribe(
            (list) => {
                if ( Array.isArray( list['root'].tbl_tpProductosGenericos ) ) {
                    this.listOne = list['root'].tbl_tpProductosGenericos;
                } else {
                    this.listOne.push(list['root'].tbl_tpProductosGenericos);
                }
                this.loading = false;
            }
        );
    }

    detail(subId, dsTpProducto ) {
        this.data.back = 'products';
        this.navCtrl.setRoot(ListSubProductPage, {
            content: this.data,
            id: subId,
            name: dsTpProducto
        });
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }
}
