import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import { ProductProvider } from '../../providers/product/product';
import { ListSubProductPage } from '../list-sub-product/list-sub-product';
import { ProductosPage } from '../productos/productos';
import { AdquierePage } from '../adquiere/adquiere';
import moment from 'moment';
import jsSHA from 'jssha';

@IonicPage()
@Component({
    selector: 'page-list-prod',
    templateUrl: 'list-prod.html',
})
export class ListProdPage {

    data: any;
    id: any;
    idReg: any;
    loading: boolean;
    tok: any;
    list = [];
    infor: boolean = true;
    selectData: any;
    dsTpProducto: string;
    dSTP: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        private _LoginProvider: LoginProvider,
        private _ProductProvider: ProductProvider,
    ) {
        this.loading = true;
        this.data = this.navParams.get('content');
        this.id = this.navParams.get('id');
        this.idReg = this.navParams.get('idReg');
        this.dsTpProducto = this.navParams.get('dsTpProducto');
        this.dsTpProducto = this.dsTpProducto.toLowerCase();
        this.dSTP = this.navParams.get('dSTP');
        this.dSTP = this.dSTP.toLowerCase();
    }

    ngOnInit() {
        console.log(1);
        this.token();
    }

    func(click) {
        this.data.back = 'products';
        this.navCtrl.setRoot(ListSubProductPage, {
            content: this.data,
            id: this.id
        });
    }

    token() {
        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['userProduct'].superUser;
                let contrasena = dat['userProduct'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');
                this._LoginProvider.login(usuario, hash, dat['userProduct'].country).subscribe(
                    (data) => {
                        this.tok = data['root'].token;
                        let time = moment().format('YYYY-MM-DD');
                        time = `${time}T00:00:00`;
                        console.log(time);
                        this.listP(this.tok, this.idReg, time);
                    }
                );
            }
        );
    }

    listP(token, id, date) {
        this._ProductProvider.product(token, id, date).subscribe(
            (res) => {
                if (res['root'] === '') {
                    this.infor = false;
                } else {
                    if (Array.isArray(res['root'].tbl_availableProducts)) {
                        this.list = res['root'].tbl_availableProducts;
                    } else {
                        this.list.push(res['root'].tbl_availableProducts);
                    }
                    this.select(this.list);
                }
                this.loading = false;
            }
        );
    }

    select(data) {
        if (Array.isArray(data)) {
            this._ProductProvider.description().subscribe(
                (res) => {
                    for (let i = 0; i < data.length; i++) {
                        for (let j = 0; j < res['data'].length; j++) {
                            if (data[i].dsSuffix === res['data'][j].dsSuffix) {
                                data[i].description = res['data'][j].description;
                                data[i].type = this.dsTpProducto;
                                data[i].subType = this.dSTP;
                            }
                        }
                    }
                    this.list = data;
                }
            );
        } else {
            console.log('no array');
        }
    }

    postData(objet) {
        this.data.back = 'products';
        this.navCtrl.setRoot(ProductosPage, {
            content: this.data,
            infor: objet,
            id: this.id,
            dsTpProducto: this.dsTpProducto,
            dSTP: this.dSTP,
            idReg: this.idReg
        });
    }

    add(objet) {
        this.id, this.idReg, this.dsTpProducto, this.dSTP;
        this.navCtrl.setRoot(AdquierePage, {
            content: this.data,
            infor: objet,
            id: this.id,
            idReg: this.idReg,
            dsTpProducto: this.dsTpProducto,
            dSTP: this.dSTP
        });
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }

    prueba() {
        //var arr = Object.values(arr[0]);
    }
}
