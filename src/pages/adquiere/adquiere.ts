import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListProdPage } from '../list-prod/list-prod';
import { ConfigProvider } from '../../providers/config/config';
import { ProductProvider } from '../../providers/product/product';
import { LoginProvider } from '../../providers/login/login';
import moment from 'moment';

@Component({
    selector: 'page-adquiere',
    templateUrl: 'adquiere.html'
})
export class AdquierePage {

    data: any;
    page: any = 'addProduct';
    infor: any;
    id: any;
    idReg: any;
    dsTpProducto: any;
    dSTP: any;
    forms: any;
    arrayForm: any = [];
    dataForm: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private _ConfigProvider: ConfigProvider,
        private _ProductProvider: ProductProvider,
        private _LoginProvider: LoginProvider
    ) {
        this.infor = this.navParams.get('infor');
        this.data = this._ConfigProvider.selectData(this.page);
        this.id = this.navParams.get('id');
        this.idReg = this.navParams.get('idReg');
        this.dsTpProducto = this.navParams.get('dsTpProducto');
        this.dSTP = this.navParams.get('dSTP');
    }

    ngOnInit() {
        let superUser = 'LVIRACOA';
        let superPass = 'NSkP/TNEbOg3XVks5qW4mzYrSCQPl1kwKfzFcWC7YB19JW4x63Sosepfs42l8hsIdHOV8YC6PbHs2teVhpRIUw==';
        this._LoginProvider.login(superUser, superPass, 46).subscribe(
            (data) => {
                this.create(data['root'].token, this.infor.cd, moment().format('YYYY-MM-DD'));
            }
        );
    }

    create(token, id, date) {
        this._ProductProvider.form(token, id, date).subscribe(
            (form) => {
                this.forms = form['root'];
                this.createForm(this.forms);
            }
        );
    }

    func() {
        this.data.title = "Productos";
        this.navCtrl.setRoot(ListProdPage, {
            content: this.data,
            id: this.id,
            dsTpProducto: this.dsTpProducto,
            dSTP: this.dSTP,
            idReg: this.idReg
        });
    }

    createForm(form) {
        this.arrayForm = this.jsonToArray(form);
        for (let i = 0; i < this.arrayForm.length; i++) {
            this.arrayForm[i] = this.jsonToArray(this.arrayForm[i]);
        }
        
        for (let f of this.arrayForm) {
            f['data'] = [];
            f.status = false;
            for (let j of f) {
                if (typeof(j) === 'string') {
                    f['title'] = j;
                } else {
                    f['data'].push(j);
                }
                
                if ( typeof(j.option) === 'object' ) {
                    if (Object.keys(j.option).length === 2) {
                        j.option = [j.option];
                    }
                }
            }
        }
        this.arrayForm[0].status = true;
        console.log(this.arrayForm);
    }

    jsonToArray(data) {
        let arr = [];
        for (let elem in data) {
            arr.push(data[elem]);
        }
        return arr;
    }

    logForm(form) {
        console.log(form.value);
    }
}