import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ConfigProvider } from '../../providers/config/config';
import { DoctorDomPage } from '../doctor-dom/doctor-dom';
import { MenuTemplatePage } from '../menu-template/menu-template';
import { CallTemplatePage } from '../call-template/call-template';
import { MapsProvider } from '../../providers/maps/maps';
import { ModalDoctor } from '../../models/modalDoctor';

import { TokenProvider } from '../../providers/token/token';
import { AsistenciasProvider } from '../../providers/asistencias/asistencias';
import { LoginProvider } from '../../providers/login/login';
//import { SeguimientoPage } from '../seguimiento/seguimiento';
import moment from 'moment';
import { HomePage } from '../home/home';
import jsSHA from 'jssha';

@Component({
    selector: 'page-modal-doc',
    templateUrl: 'modal-doc.html',
})
export class ModalDocPage {

    page: string = 'ModalDocPage';
    data: any;
    lat: number;
    lng: number;
    addressOne: any;
    addressTwo: any;
    location: any;
    private modalForm: FormGroup;
    model: ModalDoctor;
    asistencia: any;
    user: any;
    name: string;
    lastName: string;
    loading: boolean;
    ageModel: number;
    nif: number;
    email: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        public formBuilder: FormBuilder,
        public MapsProvider: MapsProvider,
        public TokenProvider: TokenProvider,
        public AsistenciasProvider: AsistenciasProvider,
        public alertCtrl: AlertController,
        public LoginProvider: LoginProvider,
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.loading = true;
    }

    ngOnInit() {
        this.modalForm = this.formBuilder.group({
            id: [''],
            namePac: ['', Validators.required],
            age: ['', [Validators.required, Validators.maxLength(2)]],
            address: ['', Validators.required],
            compAddress: [''],
            email: [''],
            cel: ['', [Validators.required, Validators.minLength(10)]],
            text: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(500)]]
        });

        let dir = JSON.parse(localStorage.getItem('dest'));
        this.addressOne = dir.address;

        let dirOne = JSON.parse(localStorage.getItem('dest'));
        this.addressOne = dirOne;
        let dir2 = this.addressOne.address;
        let res = dir2.split(",");
        this.addressOne = res[0];
        this.addressTwo = `${res[1]},${res[2]}`;
        this.location = res[1];

        this.ConfigProvider.init().subscribe(
            (res) => {
                let usuario = res['user'].superUser;
                let contrasena = res['user'].superPass;

                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');
                this.LoginProvider.login(usuario, hash).subscribe(
                    (log) => {
                        //console.log(log);
                    }
                );
            }
        );
        this.user = JSON.parse(localStorage.getItem('user'));
        let nameGlobal = this.user.code;
        let cut = nameGlobal.split(' ');
        this.name = cut[1];
        this.lastName = cut[2];

        this.model = new ModalDoctor();
        this.model.address = this.addressOne.address;
        this.model.email = this.user.email;
        this.model.nif = this.user.nif;
        this.loading = false;
        this.nif = this.user.nif;
        this.email = this.user.email;
    }

    /*VOLVER*/
    dismiss(data) {
        let contentPage = this.ConfigProvider.selectData(data);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        } else if (contentPage.type === "map") {
            this.navCtrl.setRoot(DoctorDomPage, {
                content: contentPage
            });
        }
    }

    continuar() {
        this.loading = true;
        let date = moment().format('DDMMYYYY HHmm');
        let file = `BBVACO${moment().format('YYYYMMDDHHmmssmmmmmm')}`;
        let country = JSON.parse(localStorage.getItem('country'));
        let des = JSON.parse(localStorage.getItem('dest'));

        let localCabecera = {
            'cabecera': {
                'AMA': country.countryCode,
                "CLIENT": "100060",
                "ID": "TESTBBVA",
                'FILE': file
            }
        };
        localStorage.setItem('file', JSON.stringify(localCabecera));

        /* JSON DE ASISTENCIA */
        let data = {
            'crearAsistencia': {
                'mseCrearAsistencia': {
                    'cabecera': {
                        'AMA': country.countryCode,
                        "CLIENT": "100060",
                        "ID": "TESTBBVA",
                        'FILE': file
                    },
                    'atributosFijos': {
                        'DATE': date,
                        "NAME": this.modalForm.value.namePac,
                        "SURNAME1": this.modalForm.value.namePac,
                        "POLICY": "BBVA001",
                        "RISK": "200",
                        "GUARANTEE": "B0020",
                        "CAUSE": "104001",
                        "SERVICE": "40430",
                        "BREAKDOWN": "null",
                        "COUNTRY1": country.countryCode,
                        "COUNTRY2": country.countryCode,
                        "LOCATION": `${this.modalForm.value.address} ${this.location} ${this.modalForm.value.compAddress}`,
                        "TELEPHONE": this.modalForm.value.cel,
                        "DNI": this.modalForm.value.id,
                        "SERVICE_DATE": date,
                        "MSG_CLIENT": "-",
                        "COMMENTS": this.modalForm.value.text,
                        "EMAIL": this.modalForm.value.email,
                        "REIMBURSEMENT": "N",
                        "ORIGEN_LON": des.lon,
                        "ORIGEN_LAT": des.lat,
                        "DESTINO_LON": des.lon,
                        "DESTINO_LAT": des.lat
                    },
                    "atributosVariables": [
                        {
                            "etiqueta": "MAT",
                            "valor": this.modalForm.value.id
                        },
                        {
                            "etiqueta": "IED",
                            "valor": "46"
                        },
                        {
                            "etiqueta": "G65",
                            "valor": "MASCULINO"
                        },
                        {
                            "etiqueta": "M78",
                            "valor": "PRUEBA 001 SOLICITUD SERVICIO WEB REST API GATEWAY"
                        },
                        {
                            "etiqueta": "G14",
                            "valor": "PRUEBA DIAGNOSTICO"
                        },
                        {
                            "etiqueta": "M79",
                            "valor": "PRUEBA MEDICAMENTOS"
                        },
                        {
                            "etiqueta": "BV2",
                            "valor": "CONTRATO"
                        }
                    ]
                }
            }
        };

        /* OBTENER TOKEN */
        this.TokenProvider.GetToken().subscribe(
            (result) => {
                let token = result['token'];

                this.AsistenciasProvider.createAsis(data, token).subscribe(
                    (result) => {
                        if (result['crearAsistenciaResponse'].mssCrearAsistencia.RESULT === 'OK') {
                            if (result['crearAsistenciaResponse'].mssCrearAsistencia.AMA_ASSIST) {
                                localStorage.setItem('asistencia', JSON.stringify(result['crearAsistenciaResponse'].mssCrearAsistencia));
                                localStorage.removeItem('dest');
                                localStorage.removeItem('position');
                                this.alert(`Número de asistencia:  ${result['crearAsistenciaResponse'].mssCrearAsistencia.AMA_ASSIST}`);
                                this.addFirebase(this.data.title, this.data.name);
                                this.navCtrl.setRoot(HomePage);
                            } else {
                                this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'failAsis');
                            }

                        } else {
                            this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'failAsis');
                        }
                    }, (error) => {
                        console.log(error);
                        this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'failAsis');
                    }
                );
            }, (error) => {
                console.log(error);
                this.alertFail('Hemos presentado un fallo de seguridad por favor comunícate con nuestro call center', 'failAsis');
            }
        );
    }

    /*ALERTA*/
    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: '<img src="assets/icon/correct_icon-g.svg"><h1>Su solicitud ha sido registrada exitosamente.</h1>',
            subTitle: `Dentro de 10 minutos máximo, usted podrá visualizar la ruta y tiempo estimado de llegada del proveedor que asistirá su servicio en la opción: "Seguimiento"  ubicada en la parte inferior de esta pantalla o en el menú principal<br /><br /> ${message}`,
            buttons: [
                {
                    text: 'Aceptar'
                }
            ]
        });
        alert.present();
    }

    alertFail(message, page) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            subTitle: message,
            buttons: [
                {
                    text: 'Aceptar',
                    handler: () => {
                        let contentPage = this.ConfigProvider.selectData(page);
                        this.navCtrl.setRoot(CallTemplatePage, {
                            content: contentPage
                        });
                    }
                }
            ]
        });
        alert.present();
    }

    change() {
        if (this.ageModel > 99) {
            this.ageModel = 99;
        }
    }

    addFirebase(title, name) {
        let calls = {
            click: title,
            date: moment().format('DD/MM/YYYY'),
            hour: moment().format('HH:mm'),
            icon: 'assets/icon/wellness_icon.svg',
            page: name,
        };
        if (localStorage.getItem('calls') === null) {
            let callArray = [];
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        } else {
            let callArray = JSON.parse(localStorage.getItem('calls'));
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        }
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }
}
