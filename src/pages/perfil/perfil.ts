import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PerfilEditarPage } from '../perfilEditar/perfilEditar';
import { HomePage } from '../home/home';
import { LoginProvider } from '../../providers/login/login';
import { ConfigProvider } from '../../providers/config/config';

@Component({
    selector: 'page-perfil',
    templateUrl: 'perfil.html'
})
export class PerfilPage {

    loading: boolean;
    email: string;
    name: string;
    cc: number;
    constructor(
        public navCtrl: NavController,
        public LoginProvider: LoginProvider,
        public ConfigProvider: ConfigProvider
    ) {
        this.loading = true;
    }

    ngOnInit() {
        let token = localStorage.getItem('token');
        let id = localStorage.getItem('id');
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.LoginProvider.getDetail(token, id, urls['json']).subscribe(
                    (result) => {
                        if (result['WARNING']) {
                            console.log(result['WARNING']);
                            this.loading = false;
                        } else if (result['ERROR']) {
                            console.log('error');
                            this.loading = false;
                        } else {
                            let name = result['root'].usuarios.code;
                            name = name.split(' ');
                            this.email = result['root'].usuarios.email;
                            this.name = `${name['1']}  ${name['2']}`;
                            this.cc = result['root'].usuarios.cdUsuario;
                            this.loading = false;
                        }
                    }, (error) => {
                        console.log(error);
                        this.loading = false;
                    }
                );
            });

    }

    perfilEditar() {
        this.navCtrl.push(PerfilEditarPage);
    }

    func() {
        this.navCtrl.setRoot(HomePage);
    }
}
