import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { ConveniosPage } from '../convenios/convenios';
import moment from 'moment';

@Component({
	selector: 'page-convenios-map',
	templateUrl: 'convenios-map.html',
})
export class ConveniosMapPage {

	data: any;
	detail: any;
	loading: boolean;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public ConfigProvider: ConfigProvider
	) {
		this.data = this.navParams.get('content');
		this.detail = this.navParams.get('map');
	}

	ngOnInit() {
		this.initMap(this.detail);
	}

	func(click) {
		let contentPage = this.ConfigProvider.selectData(click);
		this.navCtrl.setRoot(ConveniosPage, {
			content: contentPage
		});
	}

	initMap(data) {
		this.loading = true;
		var map;
		var marcadores = [];
		var marcadoresInfo = [];
		for (let da of data) {
			marcadores.push(da);
		}

		var myLatlng = new google.maps.LatLng(marcadores[0].lat, marcadores[0].longi);
		var mapOptions = {
			zoom: 14,
			center: myLatlng
		};

		map = new google.maps.Map(document.getElementById("map"), mapOptions);

		var infowindow = new google.maps.InfoWindow();
		var marker, i;

		for (i = 0; i < marcadores.length; i++) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(marcadores[i].lat, marcadores[i].longi),
				map: map,
				icon: marcadores[i].icon 
			});
			marcadoresInfo.push(marker);
			google.maps.event.addListener(marker, 'click', (function (marker, i) {
				return function () {
					infowindow.setContent(`<div class="infoDataFilter">
					<table>
						<tr>
							<td colspan="2"><h2>${marcadores[i].name}</h2></td>
						</tr>
						<tr>
							<td>Departamento / Ciudad</td>
							<td>${marcadores[i].city}</td>
						</tr>
						<tr>
							<td>Dirección</td>
							<td>${marcadores[i].direccion}</td>
						</tr>
						<tr>
							<td>Tipo servicio</td>
							<td>${marcadores[i].type}</td>
						</tr>
						<tr>
							<td>Horario</td>
							<td>${marcadores[i].time}</td>
						</tr>
						<tr>
							<td>Descuento</td>
							<td>${marcadores[i].des}</td>
						</tr>
						<tr>
							<td>Observaciones</td>
							<td>${marcadores[i].infor}</td>
						</tr>
					</table>
					<a href="${marcadores[i].tel}"> <img src="assets/icon/Telephone.svg" alt=""></a></p>
				</div>`);
					infowindow.open(map, marker);
				};
			})(marker, i));
		}
		marker.setMap(map);
		this.loading = false;

	}

	call(call, name) {
		let calls = {
			click: name,
			date: moment().format('DD/MM/YYYY'),
			hour: moment().format('HH:mm'),
			icon: 'assets/icon/Telephone.svg',
			page: name,
			tel: call
		};
		if (localStorage.getItem('calls') === null) {
			let callArray = [];
			callArray.push(calls);
			localStorage.setItem('calls', JSON.stringify(callArray));
		} else {
			let callArray = JSON.parse(localStorage.getItem('calls'));
			callArray.push(calls);
			localStorage.setItem('calls', JSON.stringify(callArray));
		}

	}
}
