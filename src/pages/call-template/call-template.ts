import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { MenuTemplatePage } from '../menu-template/menu-template';
import { HomePage } from '../home/home';
import { ConveniosPage } from '../convenios/convenios';
import { CreateUserPage } from '../create-user/create-user';
import { ChangePassPage } from '../change-pass/change-pass';
import moment from 'moment';

@Component({
    selector: 'page-call-template',
    templateUrl: 'call-template.html',
})
export class CallTemplatePage {

    data: any;
    logo: any;
    menu: any = [];
    form: any = [];
    viewMenu: boolean = true;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
    ) {
        this.logo = this.ConfigProvider.logo();
    }

    ngOnInit() {
        this.data = this.navParams.get('content');
        if (this.data.back === "create-user" || this.data.back === "ChangePassPage" ) {
            this.viewMenu = false;
        }
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "menu") {
            this.navCtrl.setRoot(MenuTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "call") {
            this.navCtrl.setRoot(CallTemplatePage, {
                content: contentPage
            });
        } else if (contentPage.type === "home") {
            this.navCtrl.setRoot(HomePage);
        } else if (contentPage.type === "filter") {
            this.navCtrl.setRoot(ConveniosPage, {
                content: contentPage
            });
        } else if (contentPage.type === "form") {
            if (this.data.back === 'create-user') {
                this.navCtrl.setRoot(CreateUserPage);
            } else if (this.data.back === 'ChangePassPage') {
                this.navCtrl.setRoot(ChangePassPage);
            }
        }
    }

    addFirebase(call, title, name) {
        let calls = {
            click: title,
            date: moment().format('DD/MM/YYYY'),
            hour: moment().format('HH:mm'),
            icon: 'assets/icon/Telephone.svg',
            page: name,
            tel: call
        };
        if (localStorage.getItem('calls') === null) {
            let callArray = [];
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        } else {
            let callArray = JSON.parse(localStorage.getItem('calls'));
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        }
    }

    formCall(data, title, name) {
        let iconN;
        if (name === 'AsisMedVirPage') {
            iconN = 'assets/icon/Tablet.svg';
        } else {
            iconN = 'assets/icon/Telephone.svg';
        }
        let calls = {
            click: title,
            date: moment().format('DD/MM/YYYY'),
            hour: moment().format('HH:mm'),
            icon: iconN,
            page: name,
            tel: data
        };
        if (localStorage.getItem('calls') === null) {
            let callArray = [];
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        } else {
            let callArray = JSON.parse(localStorage.getItem('calls'));
            callArray.push(calls);
            localStorage.setItem('calls', JSON.stringify(callArray));
        }
        
        window.open(data);
    }
}
