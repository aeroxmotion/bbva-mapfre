import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LlamadasPage } from '../llamadas/llamadas';

@Component({
    selector: 'page-historial',
    templateUrl: 'historial.html'
})
export class HistorialPage {

    tabHistorial1 = LlamadasPage;
    list: any;
    items: any = [];
    count: number;
    constructor(
        public navCtrl: NavController,
    ) {

    }

    ngOnInit() {
        this.listCall();
    }

    func() {
        this.navCtrl.setRoot(HomePage);
    }

    //listar
    listCall() {
        if (JSON.parse( localStorage.getItem('calls') ) === null) {
            this.count = 0;
        } else {
            this.items = JSON.parse( localStorage.getItem('calls') );
            this.count = this.items.length;
        }
    }
}