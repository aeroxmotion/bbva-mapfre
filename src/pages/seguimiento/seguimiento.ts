import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { HomePage } from '../home/home';
import { CallTemplatePage } from '../call-template/call-template';
import { TokenProvider } from '../../providers/token/token';
import { AsistenciasProvider } from '../../providers/asistencias/asistencias';

@Component({
    selector: 'page-seguimiento',
    templateUrl: 'seguimiento.html',
})
export class SeguimientoPage {

    data: any;
    page: string = 'seguimiento';
    lat: number;
    lng: number;
    seguimiento: any;
    seguimientoTwo: any;
    arrayPo: any = [];
    numberEnd: any;
    numberEndTwo: any;
    loading: boolean;
    cabecera: any;
    ama_assist: any;
    textTime: any;
    exist: boolean = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        public TokenProvider: TokenProvider,
        public alertCtrl: AlertController,
        public AsistenciasProvider: AsistenciasProvider
    ) {
        this.data = this.ConfigProvider.selectData(this.page);
        this.ama_assist = 0;
        this.loading = true;
    }

    ngOnInit() {
        this.seguimiento = JSON.parse(localStorage.getItem('asistencia'));
        if (this.seguimiento === null) {
            this.alert('No hay asistencias solicitadas');
        } else {
            var text = this.seguimiento.AMA_SERVICE.toString();
            this.ama_assist = this.seguimiento.AMA_ASSIST;
            this.seguimientoTwo = text.match(/.{1,1}/g);
            for (var i = 0; i < this.seguimientoTwo.length; i++) {
                this.arrayPo.push(`3 ${this.seguimientoTwo[i]}`);
            }
            this.numberEnd = this.arrayPo.toString();
            var str = this.numberEnd;
            var replaced = str.split(',').join('');
            this.numberEndTwo = replaced;

            this.lat = 4.6591423;
            this.lng = -74.1060528;

            this.getTokenMai();
        }
    }

    /*VOLVER*/
    dismiss(data) {
        this.navCtrl.setRoot(HomePage);
    }

    getTokenMai() {
        this.ConfigProvider.getUrl().subscribe(
            (urls) => {
                this.TokenProvider.loginSeg(urls['asis3']).subscribe(
                    (tok) => {
                        this.TokenProvider.getRec(this.numberEndTwo, urls['asis3'], tok['token']).subscribe(
                            (seg) => {
                                if (!seg['latitud_rec']) {
                                    seg['latitud_rec'] = '0';
                                    seg['longitud_rec'] = '0';  
                                }
                                let estimatedTime = '0';
                                this.TokenProvider.getEstimatedTime(tok['token'], 'es',
                                    this.stringTocor(seg['latitud_srv']), 
                                    this.stringTocor(seg['longitud_srv']),
                                    this.stringTocor(seg['latitud_rec']),
                                    this.stringTocor(seg['longitud_rec']), 
                                    urls['accsmart'].url).subscribe(
                                        estimated => {
                                            if (estimated['tiempo'] !== null) {
                                                estimatedTime = estimated['tiempo'];
                                            }

                                            let map = {
                                                'latitud_srv': this.stringTocor(seg['latitud_srv']),
                                                'longitud_srv': this.stringTocor(seg['longitud_srv']),
                                                'latitud_rec': this.stringTocor(seg['latitud_rec']),
                                                'longitud_rec': this.stringTocor(seg['longitud_rec']),
                                                'time': estimatedTime
                                            };

                                            this.exist = true;
                                            this.initMap(map);
                                        },
                                        (err) => {
                                            let map = {
                                                'latitud_srv': this.stringTocor(seg['latitud_srv']),
                                                'longitud_srv': this.stringTocor(seg['longitud_srv']),
                                                'latitud_rec': this.stringTocor(seg['latitud_rec']),
                                                'longitud_rec': this.stringTocor(seg['longitud_rec']),
                                                'time': estimatedTime
                                            };

                                            this.exist = true;
                                            this.initMap(map);
                                            console.log(err);
                                        }
                                    );
                            }
                        );
                    },
                    (error) => {
                        //this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
                        console.log(error);
                        this.loading = false;
                    }
                );
            },
            (error) => {
                this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
            }
        );
    }
    //                             let map = {
    //                                 'latitud_srv': this.stringTocor(seg['latitud_srv']),
    //                                 'longitud_srv': this.stringTocor(seg['longitud_srv']),
    //                                 'latitud_rec': this.stringTocor(seg['latitud_rec']),
    //                                 'longitud_rec': this.stringTocor(seg['longitud_rec']),
    //                                 'time': seg['tiestillega_rec']
    //                             }
    //                             if (map.time != 0) {
    //                                 this.exist = true;
    //                             }
    //                             this.initMap(map);
    //                         }
    //                     );
    //                 },
    //                 (error) => {
    //                     this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
    //                 }
    //             );
    //         },
    //         (error) => {
    //             this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
    //         }
    //     );
    // }

    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'aceptar',
                    handler: () => {
                        this.navCtrl.setRoot(HomePage);
                    }
                }
            ]
        });
        alert.present();
    }

    alertFail(message, page) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            subTitle: message,
            buttons: [
                {
                    text: 'Aceptar',
                    handler: () => {
                        let contentPage = this.ConfigProvider.selectData(page);
                        this.navCtrl.setRoot(CallTemplatePage, {
                            content: contentPage
                        });
                    }
                }
            ]
        });
        alert.present();
    }

    initMap(data) {
        var map;
        if (data.latitud_rec === 0) {
            data.latitud_rec = data.latitud_srv;
            data.longitud_rec = data.longitud_srv;
            data.icon_rec = 'assets/icon/pins/seguimiento.png';
        }else {
            data.mensaje_rec = '';
            data.icon_rec = 'assets/icon/pins/prestador.png';
        }

        if (data.time === 0) {
            data.mensaje_rec = 'No es posible posicionar recurso.';
            data.time = 'No es posible posicionar recurso.';
        } else {
            this.textTime =  data.time;
            data.time = '';
        }
        data.icon_srv = 'assets/icon/pins/seguimiento.png';
        var marcadores = [
            [data.mensaje_rec, data.latitud_srv, data.longitud_srv, data.icon_srv],
            [data.time, data.latitud_rec, data.longitud_rec, data.icon_rec]
        ];

        var myLatlng = new google.maps.LatLng(data.latitud_srv, data.longitud_srv);
        var mapOptions = {
            zoom: 16,
            center: myLatlng
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        for (var i = 0; i < marcadores.length; i++) {
            var beach = marcadores[i];
            var marker = new google.maps.Marker({
                position: { lat: beach[1], lng: beach[2] },
                label: beach[0],
                map: map,
                icon: beach[3]
            });
        }
        marker.setMap(map);
        this.loading = false;
    }

    refresh() {
        this.loading = true;
        this.getTokenMai();
    }

    update() {
        this.cabecera = JSON.parse(localStorage.getItem('file'));
        let data = {
            "actualizarAsistencia": {
                "mseActualizarAsistencia": {
                    "cabecera": {
                        "AMA": this.cabecera['cabecera'].AMA,
                        "CLIENT": this.cabecera['cabecera'].CLIENT,
                        "ID": this.cabecera['cabecera'].ID,
                        "FILE": this.cabecera['cabecera'].FILE
                    },
                    "atributosFijos": {
                        "AMA_ASSIST": this.ama_assist,
                        "ACTION": "1",
                        "COMMENTS": "PRUEBA",
                        "MSG_CLIENT": "-"
                    }
                }
            }
        };

        let warning = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: this.data.alertcancel,
            buttons: [
                {
                    text: 'Si',
                    handler: () => {
                        /* OBTENER TOKEN */
                        this.loading = true;
                        this.TokenProvider.GetToken().subscribe(
                            (result) => {
                                let token = result['token'];
                                /* ACTUALIZAR ASISTENCIA */
                                this.AsistenciasProvider.updateAsis(data, token).subscribe(
                                    (res) => {
                                        if (res['actualizarAsistenciaResponse'].mssActualizarAsistencia.RESULT === 'OK') {
                                            localStorage.removeItem('file');
                                            localStorage.removeItem('asistencia');
                                            this.loading = false;
                                            this.alert(this.data.Messagecancel);
                                        } else {
                                            this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
                                        }
                                    }, (error) => {
                                        this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
                                    }
                                );
                            }, (error) => {
                                this.alertFail('Se presento un error durante la ubicacion del prestador, por favor intentelo mas tarde o llame a nuestro contact center', 'failAsis');
                            }
                        );
                    }
                },
                {
                    text: 'No',
                    handler: () => {
                        console.log('Cancelar Servicio');
                    }
                }
            ]
        });
        warning.present();
    }

    stringTocor(string) {
        if (string === '0') {
            return 0;
        } else {
            let number = string.replace(",", ".");
            return Number(number);
        }
    }
}
