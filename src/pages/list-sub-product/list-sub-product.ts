import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { LoginProvider } from '../../providers/login/login';
import { ProductProvider } from '../../providers/product/product';
import { ListProductPage } from '../list-product/list-product';
import { ListProdPage } from '../list-prod/list-prod';
import jsSHA from 'jssha';

@IonicPage()
@Component({
    selector: 'page-list-sub-product',
    templateUrl: 'list-sub-product.html',
})
export class ListSubProductPage {

    data: any;
    logo: any;
    loading: boolean;
    id: any;
    dsTpProducto: any;
    tok: string;
    list = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ConfigProvider: ConfigProvider,
        private _LoginProvider: LoginProvider,
        private _ProductProvider: ProductProvider
    ) {
        this.loading = true;
        this.data = this.navParams.get('content');
        this.id = this.navParams.get('id');
        this.dsTpProducto = this.navParams.get('name');
    }

    ngOnInit() {
        console.log(3);
        this.token();
    }

    func(click) {
        let contentPage = this.ConfigProvider.selectData(click);
        if (contentPage.type === "listProducts") {
            this.navCtrl.setRoot(ListProductPage, {
                content: contentPage
            });
        }
    }

    token() {
        this.ConfigProvider.init().subscribe(
            (dat) => {
                let usuario = dat['userProduct'].superUser;
                let contrasena = dat['userProduct'].superPass;
                let bytes = this.str2ab(contrasena);
                let pass = new jsSHA('SHA-512', 'TEXT');
                contrasena = this.ab2str(bytes);
                pass.update(contrasena);
                let hash = pass.getHash('B64');
                this._LoginProvider.login(usuario, hash, dat['userProduct'].country).subscribe(
                    (data) => {
                        this.tok = data['root'].token;
                        this.listSp(this.tok);
                    }
                );
            }
        );
    }

    listSp(token) {
        this._ProductProvider.subList(token, this.id).subscribe(
            (sp) => {
                if (Array.isArray(sp['root'].tbl_subTpProductosGenericos)) {
                    this.list = sp['root'].tbl_subTpProductosGenericos;
                } else {
                    this.list.push(sp['root'].tbl_subTpProductosGenericos);
                }
                this.loading = false;
            }
        );
    }

    detail(idReg, dsSubTipoProducto) {
        this.data.back = 'products';
        this.navCtrl.setRoot(ListProdPage, {
            content: this.data,
            idReg: idReg,
            id: this.id,
            dsTpProducto: this.dsTpProducto,
            dSTP: dsSubTipoProducto
        });
    }

    str2ab(str) {
        var buf = new ArrayBuffer(30); // 2 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    ab2str(buf) {
        var fin = '';
        var aux = new Uint8Array(buf),
            l = aux.length,
            i = 0,
            t = 65537,
            e;
        if (l - i < t) { return String.fromCharCode.apply(null, new Uint8Array(buf)); }
        while (l - i >= t || i < l) {
            if (l - i < t) {
                e += l - i;
            } else {
                e = t + i;
            }
            var aux2 = [], h = 0;
            for (var j = i; j < e; j++) {
                aux2[h] = aux[j];
                h++;
            }
            var s = String.fromCharCode.apply(String, new Uint8Array(aux2));
            fin += s;
            if (l - i >= t) {
                i = e;
            } else {
                i = l;
            }
        }
        return fin;
    }

}
