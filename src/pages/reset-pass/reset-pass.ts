import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { PasswordProvider } from '../../providers/password/password';
import { ConfigProvider } from '../../providers/config/config';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
    selector: 'page-reset-pass',
    templateUrl: 'reset-pass.html',
})
export class ResetPassPage {

    private resetPassword;
    status: boolean;
    message: string;
    location: any;
    page: string = 'reset-pass';
    data: any;
    logo: any;
    loading: boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public PasswordProvider: PasswordProvider,
        public alertCtrl: AlertController,
        public ConfigProvider: ConfigProvider
    ) {
        this.logo = this.ConfigProvider.logo();
        this.data = this.ConfigProvider.selectData(this.page);
        this.location = JSON.parse(localStorage.getItem('country'));
        this.loading = true;
    }

    ngOnInit() {
        this.loading = false;
        this.resetPassword = this.formBuilder.group({
            usuario: ['', Validators.required],
            email: ['', Validators.required]
        });
    }

    reset() {
        this.loading = true;
        this.PasswordProvider.resetPassword(this.resetPassword.value).subscribe(
            (result) => {
                if (result['OK']) {
                    this.loading = false;
                    this.alert(result['OK']);
                    this.navCtrl.push(LoginPage);
                } else if (result['WARNING']) {
                    this.loading = false;
                    this.alert(result['WARNING']);
                } else if (result['ERROR']) {
                    this.loading = false;
                    this.alert(result['ERROR']);
                }
            }
        );
    }

    /* CANCELAR */
    cancel() {
        this.navCtrl.push(LoginPage);
    }

    alert(message) {
        let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'ok'
                }
            ]
        });
        alert.present();
    }
}
