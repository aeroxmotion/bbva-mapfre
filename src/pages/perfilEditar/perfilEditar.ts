import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';

@Component({
  selector: 'page-perfilEditar',
  templateUrl: 'perfilEditar.html'
})
export class PerfilEditarPage {

public sexoOptions: Array<{ name: string, value: string }>;

constructor(
    public navCtrl: NavController
) {
    this.loadOptions();
}

loadOptions() {
    this.sexoOptions = [
        {
            name: 'MASCULINO',
            value: 'M'
        }, {
            name: 'FEMENINO',
            value: 'F'
        }
    ];
}

perfil() {
    this.navCtrl.push( PerfilPage );
}

}