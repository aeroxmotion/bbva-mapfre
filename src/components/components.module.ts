import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule, } from '@angular/common';
import { SlideComponent } from './slide/slide';
import { LoadingComponent } from './loading/loading';
import { FaIconComponent } from './fa-icon/fa-icon.component';

@NgModule({
	declarations: [
		SlideComponent,
		LoadingComponent,
		FaIconComponent
	],
	imports: [
		CommonModule,
		IonicModule
	],
	exports: [
		SlideComponent,
		LoadingComponent,
		FaIconComponent
	]
})
export class ComponentsBbva { }
