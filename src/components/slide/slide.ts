import { Component, Input } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
@Component({
    selector: 'slide',
    templateUrl: 'slide.html'
})
export class SlideComponent {
    
    @Input() images: any;
    slide: boolean = false;

    constructor(
        public viewCtrl: ViewController,
        public navCtrl: NavController
    ) {

    }
    ngOnInit() {
        if (this.images.length > 1) {
            this.slide = true;
        }
    }
}
