import { NgModule, ErrorHandler } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, DeepLinkConfig } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';

import { PreRegistroPage } from '../pages/pre-registro/pre-registro';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { VouchersPage } from '../pages/vouchers/vouchers';
import { VoucherPage } from '../pages/voucher/voucher';
import { PerfilPage } from '../pages/perfil/perfil';
import { PerfilEditarPage } from '../pages/perfilEditar/perfilEditar';
import { HistorialPage } from '../pages/historial/historial';
import { SalirPage } from '../pages/salir/salir';
import { ChangePassPage } from '../pages/change-pass/change-pass';
import { LoginPage } from '../pages/login/login';
import { CallTemplatePage } from '../pages/call-template/call-template';
import { MenuTemplatePage } from '../pages/menu-template/menu-template';
import { InforTemplatePage } from '../pages/infor-template/infor-template';
import { SeguimientoPage } from '../pages/seguimiento/seguimiento';
import { LlamadasPage } from '../pages/llamadas/llamadas';
import { ContactoPage } from '../pages/contacto/contacto';
import { ProductosPage } from '../pages/productos/productos';
import { AdquierePage } from '../pages/adquiere/adquiere';
import { Page } from "../pages/page/page";
import { CreateUserPage } from '../pages/create-user/create-user';
import { ResetPassPage } from '../pages/reset-pass/reset-pass';
import { DoctorDomPage } from '../pages/doctor-dom/doctor-dom';
import { ModalDocPage } from '../pages/modal-doc/modal-doc';
import { ConveniosPage } from '../pages/convenios/convenios';
import { ConveniosMapPage } from '../pages/convenios-map/convenios-map';
import { ListProductPage } from '../pages/list-product/list-product';
import { ListSubProductPage } from '../pages/list-sub-product/list-sub-product';
import { ListProdPage } from '../pages/list-prod/list-prod';

import { TranslateModule, TranslateStaticLoader, TranslateLoader } from "ng2-translate";
import { Link } from './../utilidades/links';

import { ComponentsModule, MapfreErrorHandler } from 'mapfre-framework';
import { LoginProvider } from '../providers/login/login';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateUserProvider } from '../providers/create-user/create-user';
import { PasswordProvider } from '../providers/password/password';
import { ConfigProvider } from '../providers/config/config';
import { ComponentsBbva } from '../components/components.module';
import { PruebaProvider } from '../providers/prueba/prueba';
import { RecaptchaModule } from 'ng-recaptcha';

export const deepLinkConfig: DeepLinkConfig = {
    links: [
        { component: HomePage, name: 'HomePage', segment: 'home' },
        { component: Page, name: 'PageTwo', segment: 'page2' }
    ]
};

export function createTranslateLoader(http: Http): TranslateStaticLoader {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}
/*MAPS*/
import { GoogleMaps } from '@ionic-native/google-maps';
import { AgmCoreModule } from '@agm/core';
import { MapsProvider } from '../providers/maps/maps';
import { TokenProvider } from '../providers/token/token';
import { AsistenciasProvider } from '../providers/asistencias/asistencias';
import { ProductProvider } from '../providers/product/product';

@NgModule({
    declarations: [
        MyApp,
        PerfilPage,
        PerfilEditarPage,
        ContactoPage,
        HomePage,
        TabsPage,
        PreRegistroPage,
        VouchersPage,
        VoucherPage,
        HistorialPage,
        LlamadasPage,
        ProductosPage,
        AdquierePage,
        Page,
        CreateUserPage,
        ResetPassPage,
        SalirPage,
        ChangePassPage,
        LoginPage,
        DoctorDomPage,
        ModalDocPage,
        ConveniosPage,
        ConveniosMapPage,
        CallTemplatePage,
        MenuTemplatePage,
        InforTemplatePage,
        SeguimientoPage,
        ListProductPage,
        ListSubProductPage,
        ListProdPage
    ],
    imports: [
        BrowserModule,
        RecaptchaModule.forRoot(),
        IonicModule.forRoot(MyApp, {
            locationStrategy: 'hash'
        }, deepLinkConfig),
        HttpModule,
        HttpClientModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        }),
        ComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsBbva,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB7_2etUPei2P3900nj_mEOFZHowtSnL_o',
            libraries: ["places"]
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        PerfilPage,
        PerfilEditarPage,
        ContactoPage,
        HomePage,
        TabsPage,
        PreRegistroPage,
        VouchersPage,
        VoucherPage,
        HistorialPage,
        LlamadasPage,
        ProductosPage,
        AdquierePage,
        Page,
        CreateUserPage,
        ResetPassPage,
        SalirPage,
        ChangePassPage,
        LoginPage,
        DoctorDomPage,
        ModalDocPage,
        ConveniosPage,
        ConveniosMapPage,
        CallTemplatePage,
        MenuTemplatePage,
        InforTemplatePage,
        SeguimientoPage,
        ListProductPage,
        ListSubProductPage,
        ListProdPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Geolocation,
        GoogleMaps,
        Link,
        {
            provide: ErrorHandler,
            useClass: MapfreErrorHandler
        },
        {
            provide: APP_BASE_HREF,
            useValue: '/'
        },
        LoginProvider,
        CreateUserProvider,
        PasswordProvider,
        ConfigProvider,
        PruebaProvider,
        MapsProvider,
        TokenProvider,
        AsistenciasProvider,
        ProductProvider,
    ]
})
export class AppModule { }
