import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, App, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from 'ng2-translate';
import { LoginPage } from '../pages/login/login';
import { InforTemplatePage } from '../pages/infor-template/infor-template';
import { CallTemplatePage } from '../pages/call-template/call-template';
import { MenuTemplatePage } from '../pages/menu-template/menu-template';
import { HistorialPage } from '../pages/historial/historial';
import { PerfilPage } from '../pages/perfil/perfil';
import { ContactoPage } from '../pages/contacto/contacto';
import { SeguimientoPage } from '../pages/seguimiento/seguimiento';

import { ConfigProvider } from '../providers/config/config';
import { Observable } from 'rxjs';

@Component({
    templateUrl: 'app.html'
})
export class MyApp implements OnInit {

    items: Observable<any[]>;
    @ViewChild(Nav) nav: Nav;

    rootPage: any = LoginPage;
    pages: Array<{ name: string, title: string, type: any, click: string, icon: string }>;
    token: any;
    logo: any;
    list: any;

    showBtn: boolean = false;
    deferredPrompt;

    constructor(
        public app: App,
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public translate: TranslateService,
        public ConfigProvider: ConfigProvider,
    ) {
        this.handleSplashScreen();

        this.ConfigProvider.init().subscribe(
            (data) => {
                this.ConfigProvider.saveLocal(data);
            }
        );

        platform.ready().then(() => {
            statusBar.styleDefault();
            splashScreen.hide();
            translate.setDefaultLang('es');
            translate.use('es');
        });

        window.datalayer.dispositivo = this.platform.platforms().join(" - ");
        app.viewDidEnter.subscribe(
            (view) => {
                this.pageChange(view.instance.name);
            }
        );
        
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        let data = this.ConfigProvider.selectData(page.name);
        if (page.type === "off") {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            localStorage.removeItem('user');
            this.nav.setRoot(LoginPage);
        } else if (page.type === "infor") {
            this.nav.setRoot(InforTemplatePage, {
                content: data
            });
        } else if (page.type === "call") {
            this.nav.setRoot(CallTemplatePage, {
                content: data
            });
        } else if (page.type === "history") {
            this.nav.setRoot(HistorialPage);
        } else if (page.type === "profile") {
            this.nav.setRoot(PerfilPage);
        } else if (page.type === "contact") {
            this.nav.setRoot(ContactoPage);
        } else if (page.type === "mapSeg") {
            this.nav.setRoot(SeguimientoPage);
        } else if (page.type === "menu") {
            this.nav.setRoot(MenuTemplatePage, {
                content: data
            });
        }
    }

    pageChange(pageName: string): void {
        if (window.visualizarPaso) {
            window.datalayer.title = pageName;
            window.visualizarPaso({ type: 'pageview' });
        } else {
            setTimeout(() => {
                this.pageChange(pageName);
            }, 500);
        }
    }

    ngOnInit(): void {
        this.pages = JSON.parse(localStorage.getItem('menuLateral'));
        if (this.pages === null) {
            this.ConfigProvider.init().subscribe(
                (data) => {
                    this.pages = data['menuLateral'];
                }
            );
        }
    }

    // hide #splash-screen when app is ready
    async handleSplashScreen(): Promise<void> {
        try {
            // wait for App to finish loading
            await this.platform.ready();
        } catch (error) {
            console.error('Platform initialization bug');
        }

        // Any operation that shoud be performed BEFORE showing user UI,
        // in a real App that may be cookie based authentication check e.g.
        // await this.authProvider.authenticate(...)

        // fade out and remove the #splash-screen
        const splash = document.getElementById('splash-screen');
        splash.style.opacity = '0';
        setTimeout(() => { splash.remove(); }, 300);
    }
}
