export const config: any = {
    version: 2,
    logo: 'assets/imgs/bbva-logo.png',
    country: {
        id: 7,
        name: 'colombia',
        countryCode: 'CO',
    },
    menuLateral: [
        {
            name: 'asistencia',
            title: 'Familia Vital',
            type: 'menu',
            click: 'asistencia',
            icon: 'insurance_icon-w'
        },
        {
            name: 'information',
            title: 'Información',
            type: 'infor',
            click: 'informacion',
            icon: 'Info-w'
        },
        {
            name: 'perfil',
            title: 'Perfil',
            type: 'Perfil',
            click: 'perfil',
            icon: 'Profile-w'
        },
        {
            name: 'hitorial',
            title: 'Historial',
            type: 'history',
            click: 'hitorial',
            icon: 'Frequency-w'
        },
        {
            name: 'contacto',
            title: 'Contáctenos',
            type: 'contact',
            click: 'contacto',
            icon: 'Telephone-w'
        },
        {
            name: 'seguimiento',
            title: 'Seguimiento',
            type: 'mapSeg',
            click: 'seguimiento',
            icon: 'Look-Up-w'
        },
        {
            name: 'terminos',
            title: 'Términos y condiciones',
            type: 'infor',
            click: 'terminos',
            icon: 'document_icon-w'
        },
        {
            name: 'off',
            title: 'Cerrar sesión',
            type: 'off',
            click: 'off',
            icon: 'On-Off-w'
        }
    ],
    menuTabs: [
        {
            name: 't&c',
            label: 'T&C',
            click: 't&c',
            icon: 'document_icon-w',
            back: 'home'
        },
        {
            name: 'profile',
            label: 'Perfil',
            click: 'profile',
            icon: 'Profile-w',
            back: 'home'
        },
        {
            name: 'history',
            label: 'Historial',
            click: 'history',
            icon: 'Frequency-w',
            back: 'home'
        }
        ,
        {
            name: 'contact',
            label: 'Contáctenos',
            click: 'contact',
            icon: 'Telephone-w',
            back: 'home'
        }
    ],
    pages: [
        {
            type: 'form',
            name: 'login',
            title: 'Ingresa tus datos para comenzar',
            buttonLogin: 'Continuar',
            buttonRegister: 'Registrese',
            buttonReset: 'Olvidaste tu contraseña?',
            img: [
                {
                    "url": "assets/imgs/512906076.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/523171462.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/625683286.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ]
        },
        {
            type: 'form',
            name: 'create-user',
            buttonRegister: 'Registrar',
            buttonCancel: 'Cancelar',
            img: [
                {
                    "url": "assets/imgs/512906076.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/523171462.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/625683286.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ]
        },
        {
            type: 'form',
            name: 'ChangePassPage',
            buttonRegister: 'Registrar',
            buttonCancel: 'Cancelar',
            img: [
                {
                    "url": "assets/imgs/512906076.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ]
        },
        {
            type: 'form',
            name: 'reset-pass',
            buttonSend: 'Enviar',
            buttonCancel: 'Cancelar',
            img: [
                {
                    "url": "assets/imgs/512906076.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/523171462.jpg",
                    "title": "",
                    "subTitle": "",
                },
                {
                    "url": "assets/imgs/625683286.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ]
        },
        {
            name: 'home',
            type: 'home',
            menu: [
                {
                    icon: 'insurance_icon',
                    title: 'Familia Vital',
                    description: 'Protección en Vida e Incapacidad Total y Permanente, con un paquete integral de Asistencias para tu Familia.',
                    click: 'asistencia',
                    status: 1
                },
                {
                    icon: 'Info',
                    title: 'Información',
                    description: 'Lorem Ipsum es simplemente el texto relleno de imprentas y archivos de texto.',
                    click: 'information',
                    status: 0
                },
                {
                    icon: 'md-cart',
                    title: 'Adquiere',
                    description: 'Lorem Ipsum es simplemente el texto relleno de imprentas y archivos de texto.',
                    click: 'productos',
                    status: 0
                },
                {
                    icon: 'md-cart',
                    title: 'Adquiere',
                    description: 'Lorem Ipsum es simplemente el texto relleno de imprentas y archivos de texto.',
                    click: 'Adquiere',
                    status: 0
                }
            ]
        },
        {
            name: 'asistencia',
            title: ' Asistencias',
            subTitle: 'Solicitar Asistencia',
            type: 'menu',
            back: 'home',
            menu: [
                {
                    icon: 'health_icon',
                    title: 'Asistencias Médicas',
                    description: 'Garantiza tu tranquilidad y la de tu familia en todo momento. Conoce tus asistencias médicas aquí.',
                    click: 'asisMedica',
                    status: 1
                },
                {
                    icon: 'Pill',
                    title: 'Convenios',
                    description: 'Conoce nuestro programa de descuento  en servicios y cuidados de la salud.',
                    click: 'salud',
                    status: 1
                },
                {
                    icon: 'education_icon',
                    title: 'Orientación Escolar Telefónica',
                    description: 'Asesoría escolar para tus hijos en materias como  matemáticas, física, química, biología, ciencias sociales y español.',
                    click: 'asisEscolar',
                    status: 1
                }
            ],
            img: [
                {
                    "url": "assets/imgs/656434576.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ],
        },
        {
            name: 'asisMedica',
            title: 'Asistencias Médicas',
            subTitle: 'Asistencia Médica',
            type: 'menu',
            back: 'asistencia',
            menu: [
                {
                    icon: 'Telephone',
                    title: 'Orientación Médica Telefónica',
                    description: 'Atención telefónica con un profesional de la salud en todo momento.',
                    click: 'orientacion',
                    status: 1
                },
                {
                    icon: 'Tablet',
                    title: 'Asistencia Médica Virtual',
                    description: 'Atención médica calificada e inmediata con un profesional de la salud por videollamada.',
                    click: 'AsisMedVirPage',
                    status: 1
                },
                {
                    icon: 'wellness_icon',
                    title: 'Médico a Domicilio',
                    description: 'Consulta médica de emergencia desde la comodidad de tu casa.',
                    click: 'doctorDomi',
                    status: 1
                },
                {
                    icon: 'Car-Plus',
                    title: 'Traslado Médico de Emergencia',
                    description: 'Servicio de traslado en ambulancia por emergencia las 24 horas del día.',
                    click: 'traslado',
                    status: 1
                }
            ]
        },
        {
            name: 'asisEscolar',
            title: 'Orientación escolar',
            iconTitle: 'call',
            type: 'call',
            back: 'asistencia',
            img: [
                {
                    "url": "assets/imgs/803674326.jpg"
                }
            ],
            infor: 'Cuando tus hijos requieran orientación sobre materias escolares como matemáticas, física, química, biología, ciencias sociales y español.<br /><br />Comunícate sin costo con un tutor aquí.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },
        {
            name: 'AsisMedVirPage',
            title: 'Asistencia médica virtual',
            type: 'call',
            back: 'asisMedica',
            img: [
                {
                    "url": "assets/imgs/473158686.jpg",
                    "title": "",
                    "subTitle": "",
                }
            ],
            infor: 'Médico virtual...',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },
        {
            name: 'doctorDomi',
            title: 'Medico a domicilio',
            back: 'asisMedica',
            search: 'Buscar',
            button: 'Aceptar',
            type: 'map'
        },
        {
            name: 'traslado',
            title: 'Traslado médico',
            iconTitle: 'call',
            type: 'call',
            back: 'asisMedica',
            img: [
                {
                    "url": "assets/imgs/group_hiking_forest.jpeg"
                }
            ],
            infor: 'En caso de lesión o enfermedad de cualquiera de los benefi¬ciarios, que requiera manejo en un centro hospitalario, se coordinará una ambulancia para el traslado del paciente hasta la clínica o centro médico más cercano.<br>Solicítalo sin costo aquí.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },

        {
            name: 'information',
            title: 'Información',
            back: 'home'
        },
        {
            name: 'terminos',
            title: 'Términos y condiciones',
            back: 'home'
        },

        {
            name: 'VoucherPage',
            title: 'historico',
            type: 'any',
            back: 'home'
        },
        {
            name: 'pre-login',
            title: 'Ingresa tus datos para comenzar',
        },
        {
            name: 'orientación-basica',
            title: 'Orientación Médica Telefónica',
            iconTitle: 'call',
            img: [
                {
                    "url": "assets/imgs/blog_young_couple_looking_at_book_home.jpeg"
                }
            ],
            infor: 'Este es un servicio de orientación médica telefónica, sin costo. Podrás comunicarte con un profesional de la salud quien te brindará orientación médica sobre tu situación de salud o podrá aclarar diferentes inquietudes asociadas.<br>Llámanos, estamos para atenderte!',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },

        {
            name: 'orientacion',
            title: 'Orientación Básica',
            iconTitle: 'call',
            type: 'call',
            back: 'asisMedica',
            img: [
                {
                    "url": "assets/imgs/803674326.jpg"
                }
            ],
            infor: 'Este es un servicio de orientación médica telefónica, sin costo. Podrás comunicarte con un profesional de la salud quien te brindará orientación médica sobre tu situación de salud o podrá aclarar diferentes inquietudes asociadas.<br /><br />Llámanos, estamos para atenderte!',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },

        {
            name: 'ModalDocPage',
            title: 'Medico a Domicilio',
            back: 'doctorDomi',
            button: 'Solicitar'
        },
        {
            name: 'salud',
            title: 'Ahorro Salud',
            type: 'menu',
            back: 'asistencia',
            menu: [
                {
                    icon: 'audio_icon',
                    title: 'Concierge',
                    description: 'Asesoría telefónica para la identificación y gestión de solicitudes de descuentos en salud.',
                    click: 'ConveniosClick',
                    status: 1
                },
                {
                    icon: 'Pin',
                    title: 'Convenios',
                    description: 'Navega por los diferentes convenios de descuentos que tenemos para ti y selecciona el que necesites.',
                    click: 'ConciergeClick',
                    status: 1
                }
            ],
            img: [
                {
                    "url": "assets/imgs/man_sea_island_photographer.jpeg",
                    "title": "",
                    "subTitle": "",
                }
            ]
        },
        {
            name: 'ConveniosClick',
            title: 'Concierge',
            type: 'call',
            back: 'salud',
            img: [
                {
                    "url": "assets/imgs/man_working_designer.jpeg",
                    "title": "",
                    "subTitle": "",
                }
            ],
            infor: 'Gestiona tu servicio de descuento con un programa de AHORRO SALUD con precios preferenciales (descuentos del 15% hasta 50%) con una red de aliados en (Medicina General, Especialistas, Salud Dental, Laboratorios, Salud Visual, Droguerías).<br>Solicita tu servicio aquí.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },
        {
            name: 'ConciergeClick',
            title: 'ConciergeClick',
            type: '',
            back: 'salud',
            img: [
                {
                    "url": "assets/imgs/man_working_designer.jpeg",
                    "title": "",
                    "subTitle": "",
                }
            ],
            infor: 'Gestiona tu servicio de descuento con un programa de AHORRO SALUD con precios preferenciales (descuentos del 15% hasta 50%) con una red de aliados en (Medicina General, Especialistas, Salud Dental, Laboratorios, Salud Visual, Droguerías).<br>Solicita tu servicio aquí.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },
        {
            name: 'contacto',
            title: 'contáctenos',
            type: 'contact',
            img: [
                {
                    "url": "assets/imgs/couple_working_from_home_remote_laptops.jpeg",
                    "title": "",
                    "subTitle": "",
                }
            ],
            infor: 'Ahora puedes abrir tu Cuenta en Línea desde tu celular con BBVA móvil y evita las filas en la oficina. Ahora puedes abrir tu Cuenta en Línea desde tu celular con BBVA móvil y evita las filas en la oficina.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        },
        {
            name: 'seguimiento',
            title: 'seguimiento',
            subTitle: 'Numero seguimiento ',
            type: 'mapSeg',
            back: 'home',
            refresh: 'refrescar'
        },
        {
            name: 'failAsis',
            title: 'Medico A Domicilio',
            iconTitle: 'call',
            type: 'call',
            back: 'home',
            img: [
                {
                    "url": "assets/imgs/group_hiking_forest.jpeg"
                }
            ],
            infor: 'Se presentó un fallo durante la generación  de la asistencia, por favor comuníquese con nosotros.',
            cancel: 'Cancelar',
            call: [
                {
                    number: 'tel:(031)4010000',
                    text: 'Llamar'
                }
            ]
        }
    ]
};
