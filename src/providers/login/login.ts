import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';
import moment from 'moment';

@Injectable()
export class LoginProvider {

    token: string;
    headers: any;
    country: any;
    headersTwo: any;
    urls: any;
    countryS: any = {"id": 7, "name": "colombia", "countryCode": "CO"};

    constructor(
        public http: HttpClient,
        private _ConfigProvider: ConfigProvider
    ) {
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-credentials': 'true',
            'Access-Control-Allow-header': 'X-Request-Width',
            'Access-Control-Allow-methods': 'POST',
            'Accept': 'text/plain'
        };

        this.country = JSON.parse(localStorage.getItem('country'));

        if (this.country === null) {
            this.country = this.countryS;
        }

        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                this.urls = data;
            }
        );
    }

    login(usuario, contrasena, idC?) {
        if (idC === undefined) {
            idC = this.country.id;
        }
        let url = `${this.urls.json}/login`;
        let body = {
            'login': {
                'user': usuario,
                'pass': contrasena,
                'country': idC,
                'remote_addr': ''
            }
        };

        return this.http.post(url, body, {
            headers: this.headers
        });

    }

    /* DETALLES DE USUARIO */
    getDetail(token, userID, pet) {
        let url = `${pet}/getDetailUser`;
        let body = {
            "getDetailUser": {
                "token": token,
                "userID": userID
            }
        };

        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    /* ACTUALIZAR SESION */
    updateSesion(token, pet) {
        let url = `${pet}/updateSessionExpiration`;
        let body = {
            "updateSessionExpiration": {
                "token": token
            }
        };

        return this.http.post(url, body);
    }

    terms(token, id, idRegDealer) {
        let date = moment();
        let url = `${this.urls.json}/getContractsPlus`;
        let body = {
            "getContractsPlus": {
                "token": token,
                "starDate": date.subtract(2, 'years').format('MM-DD-YYYY'),
                "endDate": date.add(2, 'years').format('MM-DD-YYYY'),
                "dealerID": idRegDealer, 
                "state": "1",
                "taxID": id
            }
        };
        return this.http.post(url, body);
    }

    modifyUserPlus(token, user) {
        let url = `${this.urls.json}/modifyUserPlus`;

        let body = {
            "modifyUserPlus": {
                "token": token,
                "userID": user.id,
                "login": user.login,
                "password": "",
                "email": user.email,
                "fiscalID": "",
                "zoneID": "",
                "salesPointID": "",
                "salesManID": "",
                "lock": "0",
                "impute": "0",
                "groupList": "",
                "expatriado": "0",
                "validateAccountPhoenix": "1",
                "xmlAttributes": "",
                "acceptedConditions": "1"
            }
        };
        
        return this.http.post(url, body);
    }
}
