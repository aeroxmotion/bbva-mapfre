import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';

@Injectable()
export class CreateUserProvider {

    token: string;
    headers: any;
    country: any;
    urls: any;

    constructor(
        public http: HttpClient,
        private _ConfigProvider: ConfigProvider
    ) {
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-credentials': 'true',
            'Access-Control-Allow-header': 'X-Request-Width',
            'Access-Control-Allow-methods': 'POST',
            'Accept': 'text/plain'
        };
        this.country = JSON.parse(localStorage.getItem('country'));
        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                this.urls = data;
            }
        );
    }

    createUser(token, values) {
        let url = `${this.urls.json}/createUserPlus`;
        let body = {
            'createUserPlus': {
                'token': token,
                'login': values.id,
                'password': '',
                'email': values.email,
                'fiscalID': values.id,
                'zoneID': -1,
                'salesPointID': -1,
                'salesManID': -1,
                'lock': 0,
                'impute': 0,
                'groupList': -1,
                'validateAccountPhoenix': 1,
                'accountType': 1,
                'xmlAttributes': this.createXml(values)
            }
        };
         return this.http.post(url, body, {
             headers: this.headers
         }); 
    }

    createXml(data) {
        var xml = `<![CDATA[<root><txtIdFiscal>${data.id}</txtIdFiscal><txtFhNacimiento>${data.date}</txtFhNacimiento><txtNmAsegurado>${data.name}</txtNmAsegurado><txtApeAsegurado>${data.lastName}</txtApeAsegurado><cmbSexo>${data.gen}</cmbSexo><txtCdPostal /><txtDirAsegurado /><txtNumDireccion /><txtComplementoDireccion /><txtBarrio /><cmbPoblacion /><txtUf /><txtEstado /><txtDDDTelefono /><txtTelefono /><txtDDDMovil /><txtMovil /><txtEmail>${data.email}</txtEmail><accountType>3</accountType></root>]]>`;
        return xml;
    }
}
