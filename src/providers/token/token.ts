import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';

@Injectable()
export class TokenProvider {

    headers: any;
    headersTwo: any;
    urls: any;

    constructor(public http: HttpClient, private _ConfigProvider: ConfigProvider) {
        let user = 'MAICLIENT';
        let pass = 'M$1client';
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = new HttpHeaders(
            {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-credentials': 'true',
                'Access-Control-Allow-header': 'X-Request-Width',
                'Access-Control-Allow-methods': 'POST',
                'Accept': 'text/plain'
            }
        );

        let cadena = btoa(`${user}:${pass}`);
        this.headers = this.headers.append('Authorization', `Basic ${cadena}`);

        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                return this.urls = data;
            }
        );
    }

    /* OBTENER TOKEN */
    GetToken() {
        let data = {};
        let url = `${this.urls.asis}/login`;
        return this.http.post(url, data, {
            headers: this.headers
        });
    }

    /* OBTEER TOKEN Maiviewer*/
    getTokenMaiviewer(code: any) {
        let data = {
            "country_code": code
        };

        let url = `${this.urls.maiviewer}/login`;
        return this.http.post(url, data, {
            headers: this.headers
        });
    }

    /* SEGUIMIENTO DE ASISTENCIA */
    getPosition(number, token) {
        let url = `${this.urls.maiviewer}/api/posicion/${number}`;
        this.headersTwo = new HttpHeaders(
            {
                'Access-Control-Allow-header': ' origin, x-requested-with, accept',
                'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type,Authorization',
                'Access-Control-Expose-Headers': 'X-Mashery-Error-Code, X-Mashery-Responder',
            }
        );
        this.headers = this.headers.append('Authorization', `Bearer ${token}`);

        return this.http.get(url, {
            headers: this.headersTwo
        });
    }

    loginSeg(dir) {
        let user = 'MAICLIENT';
        let pass = 'M$1client';
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headersTwo = new HttpHeaders(
            {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-credentials': 'true',
                'Access-Control-Allow-header': 'X-Request-Width',
                'Access-Control-Allow-methods': 'POST',
                'Accept': 'text/plain'
            }
        );
        let cadena = btoa(`${user}:${pass}`);
        this.headers = this.headers.append('Authorization', `Basic ${cadena}`);
        
        let url = `${dir}/login`;
        let body = {};
        return this.http.post(url, body, {
            headers: this.headersTwo
        });
    }
    getRec(id, dir, token) {
        let url = `${dir}/posicionRecursoPresta`;
        let cod = JSON.parse(localStorage.getItem('country'));
        let body = {
            "cdPais": cod.countryCode,
            "prestacion": id
        };
        this.headersTwo = new HttpHeaders(
            {
                'Access-Control-Allow-header': ' origin, x-requested-with, accept',
                'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type,Authorization',
                'Access-Control-Expose-Headers': 'X-Mashery-Error-Code, X-Mashery-Responder',
                'Authorization': `Bearer ${token}`,
            }
        );
        return this.http.post(url, body, {
            headers: this.headersTwo
        });
    }

    getEstimatedTime(token, lan, lOri, lonOri, lDes, lonDes, dir) {
        this.headers = new HttpHeaders();
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers = this.headers.append('Access-Control-Allow-credentials', 'true');
        this.headers = this.headers.append('Access-Control-Allow-header', 'origin, x-requested-with, accept');
        this.headers = this.headers.append('Content-Type', 'application/json;charset=UTF-8');
        this.headers = this.headers.append('Authorization', `Bearer ${token}`);

        let url = `${dir}/estimatedTime`;
        let _subsidiaryConfig: any = localStorage.getItem('subsidiaryConfig');
        if (localStorage.getItem('subsidiaryConfig')) {
            _subsidiaryConfig = JSON.parse(_subsidiaryConfig);
        }

        let body = {
            "pais": _subsidiaryConfig.code,
            "idioma": lan,
            "lat_origen": lOri, 
            "lon_origen": lonOri, 
            "lat_destino": lDes, 
            "lon_destino": lonDes
        };
        
        return this.http.post(url, body, {headers: this.headers});
    }
}