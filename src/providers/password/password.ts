import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';

@Injectable()
export class PasswordProvider {

    headers: any;
    country: any;
    urls: any;

    constructor(
        public http: HttpClient,
        private _ConfigProvider: ConfigProvider
    ) {
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-credentials': 'true',
            'Access-Control-Allow-header': 'X-Request-Width',
            'Access-Control-Allow-methods': 'POST',
            'Accept': 'text/plain'
        };
        this.country = JSON.parse(localStorage.getItem('country'));
        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                this.urls = data;
            }
        );
    }

    /*RECUPERAR CONTRASEÑA*/
    resetPassword(data) {
        let url = `${this.urls.json}/resetWebUserPassword`;
        let body = {
            'resetWebUserPassword': {
                'user': data.usuario,
                'email': data.email,
                'idPais': this.country.id,
            }
        };

        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    /*ACTUALIZAR CONTRASEÑA PRIMER INGRESO*/
    updatePass(newPass) {
        let url = `${this.urls.json}/changePassword`;
        let login = localStorage.getItem('login');
        let oldPass = localStorage.getItem('oldPassword');

        let body = {
            "changePassword": {
                "oldPassword": oldPass,
                "newPassword": newPass,
                "login": login,
                "cdPais": this.country.id
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

}
