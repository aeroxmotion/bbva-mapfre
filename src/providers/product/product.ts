import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';

@Injectable()
export class ProductProvider {

    urls: any;
    headers: any;

    constructor(
        public http: HttpClient,
        private _ConfigProvider: ConfigProvider
    ) {
        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                this.urls = data;
            }
        );

        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-credentials': 'true',
            'Access-Control-Allow-header': 'X-Request-Width',
            'Access-Control-Allow-methods': 'POST',
            'Accept': 'text/plain'
        };
    }

    list(token) {
        let url = `${this.urls.json}/getProductsType`;
        let body = {
            "getProductsType": {
                "token": token
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    subList(token, id) {
        let url = `${this.urls.json}/getSubProductsType`;
        let body = {
            "getSubProductsType": {
                "token": token,
                "productTypeID": id
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    product(token, id, date) {
        let url = `${this.urls.json}/getAvailableProducts`;
        let body = {
            "getAvailableProducts": {
                "token": token,
                "subProductTypeID": id,
                "effectDate": date
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    description() {
        let url = '/assets/products.json';
        return this.http.get(url);
    }

    table(token, id) {
        let url = `${this.urls.json}/getSparePartsOfProduct`;
        let body = {
            "getSparePartsOfProduct": {
                "token": token,
                "ProductID": id
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    pdf(token, conGenerales, id) {
        let url = `${this.urls.json}/getConditionsDocument`;
        let body = {
            "getConditionsDocument": {
                "token": token,
                "documentTYPE": "2",
                "documentID": conGenerales,
                "productIP": id
            }
        };
        return this.http.post(url, body, {
            headers: this.headers
        });
    }

    form(token, id, date) {
        let url = `${this.urls.json}/getXMLProduct`;
        let data = {
            "getXMLProduct": {
                "token": token,
                "idRegProduct": id,
                "deliveryDate": date,
                "terms": "-1"
            }
        };
        return this.http.post(url, data, {
            headers: this.headers
        });
    }

}
