import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PruebaProvider {

    headers: any;
    constructor(
        public http: HttpClient,
    ) {
        let user = 'MAICLIENT';
        let pass = 'M$1client';
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = new HttpHeaders(
            {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-credentials': 'true',
                'Access-Control-Allow-header': 'X-Request-Width',
                'Access-Control-Allow-methods': 'POST',
                'Accept': 'text/plain'
            }
        );
        let cadena = btoa(`${user}:${pass}`);
        this.headers = this.headers.append('Authorization', `Basic ${cadena}`);
    }

    prueba() {
        let data = {};
        let url = `https://bbvaseguros-cf61b.firebaseio.com/root.json`;
        return this.http.post(url, data, {
            headers: this.headers
        });
    }
}
