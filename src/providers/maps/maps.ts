import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MapsProvider {

    headers: any;

    constructor(public http: HttpClient) {
        /*DECLARACIÓN DE HEADERS SIN TOKEN*/
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-credentials': 'true',
            'Access-Control-Allow-header': 'X-Request-Width',
            'Access-Control-Allow-methods': 'POST',
            'Accept': 'text/plain'
        };
    }

    map(lat, lon) {
        let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lon}&key=AIzaSyB7_2etUPei2P3900nj_mEOFZHowtSnL_o`;
        return this.http.get(url);
    }
}
