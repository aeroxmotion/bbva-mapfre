import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class GlobalEvents {
  private _emitter = new EventEmitter<any>();

  subscribe (_type, subscriber) {
    this._emitter.subscribe(({ type, value }) => {
      if (type === _type) {
        subscriber(value);
      }
    });
  }

  emit (type, value) {
    this._emitter.emit({ type, value });
  }
}
