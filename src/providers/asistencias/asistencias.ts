import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigProvider } from '../config/config';

@Injectable()
export class AsistenciasProvider {

    headers: any;
    token: string;
    urls: any;

    constructor(public http: HttpClient, private _ConfigProvider: ConfigProvider) {
        this._ConfigProvider.getUrl().subscribe(
            (data) => {
                this.urls = data;
            }
        );
    }

    createAsis(data, token) {
        this.headers = new HttpHeaders(
            {
                'Authorization': `Bearer ${token}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-credentials': 'true',
                'Access-Control-Allow-header': 'X-Request-Width',
                'Access-Control-Allow-methods': 'POST',
                'Accept': 'text/plain',
            }
        );

        let url = `${this.urls.asis}/crearAsistencia`;
        return this.http.post(url, data, {
            headers: this.headers
        });
    }

    updateAsis(data, token) {
        this.headers = new HttpHeaders(
            {
                'Authorization': `Bearer ${token}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-credentials': 'true',
                'Access-Control-Allow-header': 'X-Request-Width',
                'Access-Control-Allow-methods': 'POST',
                'Accept': 'text/plain',
            }
        );

        let url = `${this.urls.asis}/actualizarAsistencia`;
        return this.http.post(url, data, {
            headers: this.headers
        });
    }
}
