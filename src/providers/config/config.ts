import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ConfigProvider {

    config: any;
    data: any;
    token: any;
    constructor(
        public http: HttpClient,
    ) {

    }

    init() {
        let url = "assets/json/config.json";
        return this.http.get(url);
    }

    saveLocal(data) {
        if (localStorage.getItem('version') === null || localStorage.getItem('version') !== data.version) {
            localStorage.setItem('version', JSON.stringify(data.version));
            localStorage.setItem('logo', data.logo);
            localStorage.setItem('country', JSON.stringify(data.country));
            localStorage.setItem('menuLateral', JSON.stringify(data.menuLateral));
            localStorage.setItem('menuTabs', JSON.stringify(data.menuTabs));
            localStorage.setItem('pages', JSON.stringify(data.pages));
            localStorage.setItem('departamentos', data.departments);
            localStorage.setItem('terms', data.terms);
        }
    }

    selectData(namePage) {
        this.config = JSON.parse(localStorage.getItem('pages'));
        if (this.config === null) {
            return false;
        } else {
            for (let page of this.config) {
                if (page.name === namePage) {
                    this.data = page;
                    return this.data;
                }
            }
        }

    }

    logo() {
        let logo = localStorage.getItem('logo');
        return logo;
    }

    getToken() {
        if (localStorage.getItem('token') === null) {
            return false;
        } else {
            return true;
        }
    }

    /* CONVENIOS */
    departments() {
        let url = localStorage.getItem('departamentos');
        return this.http.get(url);
    }

    getData(data) {
        return this.http.get(data);
    }

    getUrl() {
        let url = "assets/environment.json";
        return this.http.get(url);
    }
}
